﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.dotNetExtensions
{
    public static class EnumExtension
    {
        public static bool HasFlag(this Enum value, Enum flag)
        {
            int thing = Convert.ToInt32(value);
            int otherThing = Convert.ToInt32(flag);

            return (thing & otherThing) == otherThing;
        }
    }
}
