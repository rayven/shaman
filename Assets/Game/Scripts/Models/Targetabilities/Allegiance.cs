﻿namespace Scripts.Models
{
    public enum Allegiance
    {
        Good,
        Bad,
        Neutral
    }
}
