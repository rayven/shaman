﻿using System;

namespace Scripts.Models.Targetabilities
{
    [Flags]
    public enum TargettingCriterions
    {
        Allies = 0x1,
        Foes = 0x2,
        Sufferable = 0x4,
        SpellEffect = 0x8,
    }
}
