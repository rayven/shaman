﻿using System.Collections.Generic;
using Newtonsoft.Json;
using System;
using System.Linq;
using Assets.Game.Scripts.Models.Resolvers.Builders;
using Scripts.Models.viewModels;
using Scripts.Services.Loot;
using Scripts.Models.Resolvers;
using Scripts.Models.Targetabilities;
using Scripts.Utils.Constants;

namespace Scripts.Models.Cards
{
    public class Card 
        : ISelectable
        , ILootable
    {
        public Guid Guid { get; private set; }
        public List<IRequiredTargets> ActionResolvers { get; private set; }
        public string Title { get; private set; }
        public string Description { get; private set; }
        public string Image { get; private set; }
        public TargettingCriterions SelfCriterions { get; private set; }
        public TargettingCriterions TargetCriterions { get; private set; }
        public Allegiance Allegiance { get; private set; }

        public Func<ISelectable, bool> AdditionalValidations { get; set; }

        public float Cooldown { get; set; }

        [JsonIgnore]
        public bool IsSelected { get; set; }
        [JsonIgnore]
        public bool IsTargetable { get; set; }
        [JsonIgnore]
        public bool HasBeenPlayed { get; set; }

        public Card() { Allegiance = Allegiance.Good; }

        public Card(
            Guid guid,
            IEnumerable<IRequiredTargets> actionResolvers,
            string title,
            string description,
            string image,
            float cooldown,
            TargettingCriterions selfCriterions,
            TargettingCriterions target,
            Func<ISelectable, bool> additionalValidations)
        {
            Guid = guid;
            ActionResolvers = actionResolvers.ToList();
            AdditionalValidations = additionalValidations;
            Title = title;
            Description = description;
            Image = image;
            Cooldown = cooldown;
            SelfCriterions = selfCriterions;
            TargetCriterions = target;
        }

        public void Select()
        {
            IsSelected = true;
        }

        public void UnSelect()
        {
            IsSelected = false;
        }

        public IEnumerable<IRequiredTargets> GetActionResolvers()
        {
            return ActionResolvers;
        }

        public void Dispose()
        {
            //Put in bag
        }

        public LootInformation GetLootInformation()
        {
            return new LootInformation
            {
                Title = Title,
                Type = LootableTypes.Card
            };
        }
    }
}
