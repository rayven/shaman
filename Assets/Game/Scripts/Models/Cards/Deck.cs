﻿using System;
using System.Collections.Generic;
using System.Linq;
using Scripts.Models.Events;

namespace Scripts.Models.Cards
{
    public class Deck : ISubscriber<BattleStarted>
    {
        private List<Card> _remainder;
        private List<Card> _deckList;
        private Random _random;

        public bool IsEmpty { get { return !IsNotEmpty; } }
        public bool IsNotEmpty { get { return _remainder.Any(); } }

        public Deck(IEnumerable<Card> cards)
        {
            _remainder = new List<Card>(cards);
            _deckList = new List<Card>(cards);
            _random = new Random();
            Shuffle();
        }

        public void Reset()
        {
            _remainder.Clear();
            _remainder.AddRange(_deckList);
            Shuffle();
        }

        //Fisher-Yates shuffle
        public void Shuffle()
        {
            int n = _remainder.Count;
            for (int i = 0; i < n; i++)
            {
                int r = i + (int)(_random.NextDouble() * (n - i));
                Card t = _remainder[r];
                _remainder[r] = _remainder[i];
                _remainder[i] = t;
            }
        }

        public List<Card> Pick(ushort amount)
        {
            var cards = new List<Card>();

            for (int i = 0; i< amount; i++)
            {
                cards.Add(Pick());
            }

            return cards;
        }

        public Card Pick()
        {
            Card drawnCard = _remainder.First();
            _remainder.RemoveAt(0);

            return drawnCard;
        }

        public void OnEvent(BattleStarted e)
        {
            Reset();
        }
    }
}
