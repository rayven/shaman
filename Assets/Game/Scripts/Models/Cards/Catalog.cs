﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Game.Scripts.Models.Resolvers.Builders;
using Scripts.Models.Events;
using Scripts.Models.Actors;
using Scripts.Models.Cards;
using Scripts.Models.Resolvers.CardsResolvers;
using Scripts.Services.Spells;
using Scripts.Models.Resolvers;
using Scripts.Models.Targetabilities;
using Scripts.Models.Elements;
using Scripts.Utils.Constants;

namespace Assets.Game.Scripts.Models.Cards
{
    public class Catalog
    {
        private RawCatalog Raw { get; set; }
        private Actor Caster { get; set; }
        private IEventManager EventManager { get; set; }
        
        public Catalog(Actor caster, IEventManager eventManager, IEnumerable<Card> RawCards)
        {}

        public class RawCatalog
        {
            private Ally _maiken;
            private IEventManager _eventManager;
            private IVFXService _vfxService;
            private IActionResolverBuilder _builder;
            public Dictionary<Guid, Card> AllCards { get; private set; }
            
            public RawCatalog(Ally player, IEnumerable<Card> RawCards, IEventManager eventManager, IVFXService vfxService)
            {
                _maiken = player;
                _eventManager = eventManager;
                _vfxService = vfxService;
                //AllCards = RawCards.ToDictionary(card => card.Guid);
                AllCards = new Dictionary<Guid, Card>();
                _builder = new ActionResolverBuilder();
                GenerateCards();
            }

            #region "Generateur de cartes, pourra etre flushed bientot"
            private void GenerateCards()
            {
                InsertCard(BurningFlame());
                InsertCard(Nourish());
                InsertCard(LoneWolfsAspect());
            }
            private void InsertCard(Card card) { AllCards.Add(card.Guid, card); }


            private Card BurningFlame()
            {
                var actionResolver =
                    _builder
                        .ActionResolver(new ProjectileSpellActionResolver(
                            _vfxService, 
                            20, 
                            Element.Fire,
                            _eventManager, 
                            ParticlePaths.FireBall, 
                            ParticlePaths.Explosion))
                         .WithSource(_maiken);

                return new Card(
                    Guid.NewGuid(),
                    new List<IRequiredTargets>() { actionResolver },
                    "Burning Flames",
                    "GET REKT!",
                    "Path/Pour/Image/Feu",
                    2,
                    TargettingCriterions.SpellEffect,
                    TargettingCriterions.Foes,
                    (s) => true
                 );
            }

            private Card Nourish()
            {
                var actionResolver = _builder
                    .ActionResolver(
                        new HealActionResolver(
                            _vfxService,
                            10,
                            Element.Fire,
                            _eventManager))
                    .WithSource(_maiken);

                return new Card(
                    Guid.NewGuid(),
                    new List<IRequiredTargets>() { actionResolver },
                    "Nourish",
                    "Get up, stand up! Stand up for your rights!",
                    "Path/Pour/Image/Feuilles",
                    2,
                    TargettingCriterions.SpellEffect,
                    TargettingCriterions.Allies,
                    (s) => true
                 );
            }

            private Card LoneWolfsAspect()
            {
                var actionResolver = _builder
                    .ActionResolver(
                        new ResetCooldownResolver(
                            _vfxService,
                            Element.Stone,
                            _eventManager))
                    .WithSource(_maiken);

                return new Card(
                    Guid.NewGuid(),
                     new List<IRequiredTargets>() { actionResolver },
                     "Lone Wolf's Aspect",
                     "GET REKT!",
                     "Path/Pour/Image/T-Shirt-de-loup",
                     2,
                     TargettingCriterions.SpellEffect,
                     TargettingCriterions.Allies,
                        (s) => true

                 );
            }

            #endregion
        }
    }
}
