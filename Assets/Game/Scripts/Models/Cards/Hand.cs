﻿using System;
using System.Diagnostics;
using Scripts.Models.Actors;
using Scripts.Models.Events;
using Scripts.Utils.Constants;

namespace Scripts.Models.Cards
{
    public class Hand : 
        ISubscriber<BattleStarted>,
        ISubscriber<BattleEnded>, 
        ISubscriber<PickCardRequested>,
        ISubscriber<CardPlayed>,
        ISubscriber<PickPhaseFinished>
    {
        public IEventManager EventManager { get; set; }
        public event CardsChangedEventHandler HandChanged;
        public Card[] Cards { get; set; }
        public int CardCount { get; set; }
        private bool _isPickPhaseFinished;

        public Hand()
        {
            Cards = new Card[Constants.MaximumHandSize];
            CardCount = 0;
        }
        
        public void OnEvent(BattleEnded e)
        {
            ResetHand();
        }

        public void OnEvent(PickCardRequested e)
        {
            HandlePickCardRequest(e.Ally);
        }

        private void HandlePickCardRequest(Ally ally)
        {
            if (ally.Deck.IsNotEmpty && IsHandNotFull())
            {
                if (_isPickPhaseFinished)
                {
                    ally.StartCooldown(Constants.CardPickCooldown);
                }
                PickCard(ally.Deck);
                OnHandChanged(Cards);
            }
        }
        
        private bool IsHandFull()
        {
            return CardCount >= Constants.MaximumHandSize;
        }

        private bool IsHandNotFull()
        {
            return !IsHandFull();
        }

        private void PickCard(Deck deck)
        {
            var card = deck.Pick();
            Cards[CardCount++] = card;

            EventManager.CurrentEventAggregator.Publish(new CardPicked(card));

            if (IsHandFull())
            {
                EventManager.CurrentEventAggregator.Publish(new HandIsFull());
            }
        }

        private void ResetHand()
        {
            for (var i = 0; i < CardCount; i++)
            {
                Cards[i] = null;
            }
        }

        public void OnEvent(CardPlayed e)
        {
            PlayCard(e.Card);
        }

        public Card GetCard(int index)
        {
            return Cards[index];
        }

        public void PlayCard(Card card)
        {
            var needsReverse = false;
            for (var i = 0; i < CardCount; i++)
            {
                if (Cards[i] == card)
                {
                    needsReverse = true;
                }

                if (needsReverse && i < LastIndex())
                {
                    Cards[i] = Cards[i + 1];
                }

                if (needsReverse && i == LastIndex())
                {
                    Cards[i] = null;
                }
            }

            CardCount--;
            OnHandChanged(Cards);
        }

        private int LastIndex()
        {
            return CardCount - 1;
        }

        public void OnEvent(PickPhaseFinished e)
        {
            _isPickPhaseFinished = true;
        }

        public void OnEvent(BattleStarted e)
        {
            _isPickPhaseFinished = false;
        }

        protected virtual void OnHandChanged(Card[] cards)
        {
            HandChanged(this, cards);
        }
    }

    public delegate void CardsChangedEventHandler(object sender, Card[] cards);
}