﻿using System;
using System.Collections.Generic;
using Scripts.Models.Actors;
using Scripts.Models.Events;
using Scripts.Models.viewModels;
using UnityEngine.EventSystems;

namespace Scripts.Models.Blackboards
{
    public class BattleBlackboard
        : ISubscriber<BattleEnded>
        , ISubscriber<AllyAdded>
        , ISubscriber<EnemyAdded>
    {
        public List<ISelectable> Allies { get; private set; }
        public List<ISelectable> Enemies { get; private set; }

        public BattleBlackboard()
        {
            Allies = new List<ISelectable>();
            Enemies = new List<ISelectable>();
        }

        public void OnEvent(BattleEnded e)
        {
            Allies.Clear();
            Enemies.Clear();
        }

        public void OnEvent(EnemyAdded e)
        {
            Enemies.Add(e.Enemy);
        }

        public void OnEvent(AllyAdded e)
        {
            Allies.Add(e.Ally);
        }
        
    }
}
