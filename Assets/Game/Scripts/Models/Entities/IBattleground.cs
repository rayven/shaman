﻿using UnityEngine;

namespace Scripts.Models
{
    public interface IBattleground
    {
        Vector3 Position { get; set; }
        Vector3 EnemyPosition { get; set; }
        Vector3 MaikenPosition { get; set; }
    }

    public enum BattlegroundState
    {
        SideScroller,
        Battle
    }
}
