﻿using UnityEngine;

namespace Assets.Game.Scripts.Models.Entities
{
    public delegate void TotemPartsUpdatedEventHandler(object sender);

    public class Totem
    {
        public Vector3 Position { get; set; }
        private ushort _count;
        private TotemPart[] _parts;

        public event TotemPartsUpdatedEventHandler TotemPartsUpdated; 

        public Totem(Vector3 position, ushort size)
        {
            _count = 0;
            _parts = new TotemPart[size];

            Position = position;
        }

        public void AddPart(TotemPart part)
        {
            _parts[_count++] = part;
            TotemPartsUpdated(this);
        }

        public void Reset()
        {
            _parts = null;
        }
    }
    
    public class TotemPart
    {
        
    }
}
