﻿using System.Collections.Generic;
using Scripts.Models.Cards;
using Scripts.Models.Events;

namespace Scripts.Models.Entities
{
    public class Bag : ISubscriber<AddLootableToBagRequested>
    {
        private IEventManager _eventManager;
        private uint _capacity;
        private List<Card> _cards;

        public Bag(IEventManager eventManager, uint capacity)
        {
            _eventManager = eventManager;
            _capacity = capacity;
            _cards = new List<Card>();
        }

        public void OnEvent(AddLootableToBagRequested e)
        {
            if (_cards.Count < _capacity)
            {
                _cards.Add(e.Lootable as Card);
                return;
            }
            
            _eventManager.CurrentEventAggregator.Publish(new AddLootableRejected());
        }
    }
}
