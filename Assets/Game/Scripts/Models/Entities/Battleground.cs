﻿using System.Collections.Generic;
using Scripts.Models.Actors;
using Scripts.Models.Events;
using Scripts.Models.Resolvers;
using Scripts.Services.Loot;
using UnityEngine;
using Scripts.Models.Elements;

namespace Scripts.Models
{
    public class Battleground : IBattleground, 
        ISubscriber<PlayerEntered>, 
        ISubscriber<PlayerWon>,
        ISubscriber<PickPhaseFinished>
    {
        private readonly IActionResolver _victoryActionResolver;
        private readonly Dictionary<ILootable, float> _lootablesWithPercentageChances; 
        private readonly Ally _maiken;
        private readonly Ally _torn;

        public Vector3 Position { get; set; }
        public Vector3 EnemyPosition { get; set; }
        public Vector3 MaikenPosition { get; set; }
        public Vector3 TornPosition { get; set; }
        public IEventAggregator EventAggregator { get; set; }
        public Element Environment { get; set; }

        public Battleground(
            Ally maiken, 
            Ally torn, 
            IEventAggregator eventAggregator, 
            Dictionary<ILootable, float> lootables,
            IActionResolver victoryActionResolver)
        {
            _maiken = maiken;
            _torn = torn;
            EventAggregator = eventAggregator;
            _lootablesWithPercentageChances = lootables;
            _victoryActionResolver = victoryActionResolver;
            EventAggregator.Subscribe(this);
            EventAggregator.Subscribe(victoryActionResolver);
        }
        
        public void OnEvent(PlayerEntered e)
        {
            _maiken.BattlePosition = MaikenPosition;
            _torn.BattlePosition = TornPosition;
        }

        public void OnEvent(PlayerWon e)
        {
            EventAggregator.Publish(new LootCalculationRequested(_lootablesWithPercentageChances));
        }

        public void OnEvent(PickPhaseFinished e)
        {
            EventAggregator.Publish(new ActionResolverPushed(
                new List<IActionResolver>()
                {
                    _victoryActionResolver
                }));
            EventAggregator.Publish(new SpawnTotemBasesRequested(this));
        }
    }
}
