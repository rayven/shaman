﻿using Scripts.Behaviours.Look;
using Scripts.Behaviours.Navigation;
using UnityEngine;

namespace Scripts.Models.Actors
{
    public interface ICharacter
    {
        Vector3 Position { get; set; }
        float Speed { get; set; }

        INavigationBehaviour NavigationBehaviour { get; set; }
        ILookBehaviour LookBehaviour { get; set; }

        void Update();
    }
}
