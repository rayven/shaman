﻿using System;
using Scripts.Behaviours.Attack;
using UnityEngine;
using Scripts.Behaviours.Look;
using Scripts.Models.Elements;
using Scripts.Models.Targetabilities;

namespace Scripts.Models.Actors
{
    public class Actor
    {
        public ElementsSystem ElementsSystem { get; set; }
        public Vector3 Position { get; set; }
        public Vector3 HandsPosition { get; set; }
        public Vector3 CooldownIndicatorPosition { get; set; }
        public event StateChangedEventHandler StateChanged;
        public event HealthChangedEventHandler HealthChanged;
        public event CooldownStartedEventHandler CooldownStarted;

        public long MaxHealth { get; set; }
        public long BaseDamage { get; set; }
        public long Damage { get; set; }

        public Allegiance Allegiance { get; set; }
        public TargettingCriterions SelfCriterions { get; set; }
        public TargettingCriterions TargetCriterions { get; set; }
        
        public bool IsTargetable { get; set; }
        public bool IsOnCooldown { get; set; }
        public bool IsNotOnCooldown { get { return !IsOnCooldown; } }
        public bool IsDead { get; set; }
        public bool IsAlive { get { return !IsDead; } }
        public bool IsCasting { get; set; }

        public IAttackBehaviour AttackBehaviour { get; set; }
        public ILookBehaviour LookBehaviour { get; set; }

        public float TimeSpan { get; set; }
        public float Speed { get; set; }

        private ActorState _currentState;
        private long _currentHealth;

        public Actor(
            ElementsSystem elementsSystem,
            IAttackBehaviour attackBehaviour,
            ILookBehaviour lookBehaviour,
            long health, 
            long damage,
            float speed)
        {
            MaxHealth = health;
            _currentHealth = health;

            BaseDamage = damage;
            Damage = damage;
            AttackBehaviour = attackBehaviour;
            LookBehaviour = lookBehaviour;

            ElementsSystem = elementsSystem;
            Speed = speed;
        }

        public long CurrentHealth
        {
            get { return _currentHealth; }
            set
            {
                _currentHealth = value;
                OnHealthChanged(_currentHealth);
            }
        }

        public ActorState CurrentState
        {
            get { return _currentState; }
            set
            {
                _currentState = value;
                OnStateChanged(_currentState);
            }
        }

        protected virtual void OnStateChanged(ActorState state)
        {
            StateChanged(this, state);
        }

        protected virtual void OnHealthChanged(long currentHealth)
        {
            HealthChanged(this, currentHealth);
        }

        protected virtual void OnCooldownStarted(float cooldown)
        {
            CooldownStarted(this, cooldown);
        }

        public float GetNormalisedHealth()
        {
            return (float)CurrentHealth / MaxHealth;
        }

        public void StartCooldown(float cooldown)
        {
            var modifier = ElementsSystem.CalculateModifier(Stats.Speed, Stats.Environment);

            IsOnCooldown = true;
            OnCooldownStarted(cooldown * modifier);
        }

        public void ResetCooldown()
        {
            IsOnCooldown = false;
        }

        public void Revive()
        {
            Damage = BaseDamage;
            CurrentHealth = MaxHealth;
            IsDead = false;
            IsOnCooldown = false;
        }

        public void Attack(float cooldown, Action callback)
        {
            if (IsNotOnCooldown && IsAlive)
            {
                AttackBehaviour.Attack(callback);
                StartCooldown(cooldown);
            }
        }

        public void Cast(Action callback)
        {
            IsCasting = true;
            AttackBehaviour.Cast(callback);
        }

        private void CheckIfDead()
        {
            if (CurrentHealth <= 0) {
                Die();
            }
        }

        private void Die()
        {
            IsDead = true;
        }

        #region apply

        public void ApplyToHealth(float amount, Element element)
        {
            var modifier = ElementsSystem.CalculateModifier(element, Stats.Defense);
            CurrentHealth += (long)(amount * modifier);
            if (CurrentHealth > MaxHealth)
            {
                CurrentHealth = MaxHealth;
            }
            CheckIfDead();
        }

        public void ApplyToDamage(Func<long, long> applier)
        {
            Damage = applier(Damage);
        }

    #endregion
    }

    public enum ActorState
    {
        Idle,
        Running,
        Jumping,
        Patrol,
        Chase,
        Battle,
        Attacking,
        Dead
    }

    public delegate void StateChangedEventHandler(object sender, ActorState state);
    public delegate void HealthChangedEventHandler(object sender, long currentHealth);
    public delegate void CooldownStartedEventHandler(object sender, float elapseTime);
}
