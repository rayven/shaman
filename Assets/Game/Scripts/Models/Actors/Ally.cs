﻿using Scripts.Models.Elements;
using Scripts.Models.Targetabilities;
using Scripts.Behaviours.Attack;
using Scripts.Behaviours.Look;
using Scripts.Behaviours.Movement;
using Scripts.Behaviours.Navigation;
using Scripts.Behaviours.Teleport;
using Scripts.Models.Cards;
using Scripts.Models.Events;
using Scripts.Models.viewModels;
using System.Collections.Generic;
using System.Linq;
using Assets.Game.Scripts.Models.Resolvers.Builders;
using UnityEngine;

namespace Scripts.Models.Actors
{
    public class Ally 
        : Actor
        , ICharacter
        , ISelectable
        , ISubscriber<BattleStarted>
        , ISubscriber<TargetRequested>
        , ISubscriber<BattleEnded>
        , ISubscriber<ResetTargetablesRequested>
        , ISubscriber<PickPhaseFinished>
    {
        [Inject]
        public IEventManager EventManager { get; set; }
        
        public bool IsMoving { get; set; }
        public bool IsJumping { get; set; }
        public bool IsGrounded { get; set; }
        public bool IsSelected { get; set; }

        public float JumpPower { get; set; }

        public Vector3 BattlePosition { get; set; }
        public Vector3 Direction { get; set; }

        public IMoveBehaviour MoveBehaviour { get; set; }
        public ITeleportBehaviour TeleportBehaviour { get; set; }
        public INavigationBehaviour NavigationBehaviour { get; set; }

        public Deck Deck { get; set; }
        private List<IRequiredTargets> _actionResolvers;

        private Vector3 _lastPosition;

        public Ally(
            ElementsSystem elementsSystem,
            IEnumerable<IRequiredSource> actionResolvers,
            IAttackBehaviour attackBehaviour,
            IMoveBehaviour moveBehaviour,
            INavigationBehaviour navigationBehaviour,
            ILookBehaviour lookBehaviour,
            ITeleportBehaviour teleportBehaviour,
            TargettingCriterions actorTargetability,
            TargettingCriterions target,
            float speed, float jumpPower,
            long health, long baseDamage) 
            : base(elementsSystem, attackBehaviour, lookBehaviour, health, baseDamage, speed)
        {
            JumpPower = jumpPower;
            IsMoving = false;
            IsJumping = false;
            IsGrounded = true;
            MoveBehaviour = moveBehaviour;
            NavigationBehaviour = navigationBehaviour;
            LookBehaviour = lookBehaviour;
            TeleportBehaviour = teleportBehaviour;
            SelfCriterions = actorTargetability;
            TargetCriterions = target;
            Allegiance = Allegiance.Good;
           
            _actionResolvers = actionResolvers.Select(ar => ar.WithSource(this)).ToList();
        }

        public void Update()
        {
            switch (CurrentState)
            {
                case ActorState.Running:
                    if(_lastPosition != Position)
                    {
                        _lastPosition = Position;
                    }
                    else
                    {
                        CurrentState = ActorState.Idle;
                    }
                    break;
                case ActorState.Battle:
                    Battle();
                    break;
            }

            MoveBehaviour.Move(Direction, Speed);
            Direction = Vector3.zero;
        }

        public void Move(Vector3 direction)
        {
            CurrentState = ActorState.Running;
            Direction = direction;
        }

        public void Jump()
        {
            IsJumping = true;
        }

        private void GoToBattlePosition()
        {
            TeleportBehaviour.Teleport(BattlePosition);
            LookBehaviour.Look(Vector3.right * 100);
            CurrentState = ActorState.Battle;
        }

        private void Battle()
        {
            if (IsDead)
            {
                CurrentState = ActorState.Dead;
                EventManager.CurrentEventAggregator.Publish(new AllyDowned(this));
            }

            LookBehaviour.Look(Vector3.right * 100);
        }

        public void Select()
        {
            IsSelected = true;
        }

        public void UnSelect()
        {
            IsSelected = false;
        }

        public IEnumerable<IRequiredTargets> GetActionResolvers()
        {
            return _actionResolvers;
        }

        public void OnEvent(BattleStarted e)
        {
            CurrentState = ActorState.Idle;
            GoToBattlePosition();
        }

        public void OnEvent(BattleEnded e)
        {
            CurrentState = ActorState.Idle;
            IsOnCooldown = false;
            IsTargetable = false;
            IsSelected = false;
            IsCasting = false;
            Revive();
        }

        public void OnEvent(ResetTargetablesRequested e)
        {
            IsTargetable = false;
        }

        public void OnEvent(PickPhaseFinished e)
        {
            EventManager.CurrentEventAggregator.Publish(new AllyAdded(this));
        }
        
        public void OnEvent(TargetRequested e)
        {
            if (this.ValidAllegiance(e))
            {
                IsTargetable = true;
            }
        }
    }
}
