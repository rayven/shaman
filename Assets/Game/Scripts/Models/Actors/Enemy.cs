﻿using System;
using System.Collections.Generic;
using Assets.Game.Scripts.Models.Resolvers.Builders;
using Scripts.Behaviours.Attack;
using Scripts.Behaviours.Chase;
using Scripts.Behaviours.Look;
using Scripts.Behaviours.Navigation;
using Scripts.Behaviours.Patrol;
using Scripts.Behaviours.Teleport;
using Scripts.Models.AI;
using Scripts.Models.Blackboards;
using Scripts.Models.Events;
using Scripts.Models.Resolvers;
using Scripts.Models.viewModels;
using UnityEngine;
using Scripts.Models.Targetabilities;
using Scripts.Models.Elements;

namespace Scripts.Models.Actors
{
    public class Enemy 
        : Actor
        , ICharacter
        , ISelectable,
        ISubscriber<BattleEnded>,
        ISubscriber<BattleStarted>,
        ISubscriber<PickPhaseFinished>,
        ISubscriber<PlayerEntered>, 
        ISubscriber<PlayerExited>, 
        ISubscriber<TargetRequested>,
        ISubscriber<ResetTargetablesRequested>
    {
        public BattleBlackboard BattleBlackboard { get; set; }

        private readonly IEventAggregator _eventAggregator;

        public Ally Maiken { get; set; }
        public Ally Torn { get; set; }
        
        public bool IsSelected { get; set; }

        public IPatrolBehaviour     PatrolBehaviour { get; set; }
        public IChaseBehaviour      ChaseBehaviour { get; set; }
        public INavigationBehaviour NavigationBehaviour { get; set; }
        public ITeleportBehaviour   TeleportBehaviour { get; set; }
        
        public Vector3 BattlePosition { get; set; }
        public Vector3 CurrentTargetPosition { get; set; }

        public ushort MinSightDistance { get; set; }

        private Dictionary<IAIComponent, IRequiredTargets> _AIComponents;
        
        private bool _isReadyToBattle;

        public Enemy(
            ElementsSystem elementsSystem,
            IEventAggregator eventAggregator,
            IAttackBehaviour attackBehaviour,
            IChaseBehaviour chaseBehaviour,
            IPatrolBehaviour patrolBehaviour,
            INavigationBehaviour navigationBehaviour,
            ILookBehaviour lookBehaviour,
            ITeleportBehaviour teleportBehaviour,
            TargettingCriterions actorTargetability,
            TargettingCriterions target,
            float speed, ushort minSightDistance,
            long health, long baseDamage) 
            : base(elementsSystem, attackBehaviour, lookBehaviour, health, baseDamage, speed)
        {
            MinSightDistance = minSightDistance;
            ChaseBehaviour = chaseBehaviour;
            PatrolBehaviour = patrolBehaviour;
            NavigationBehaviour = navigationBehaviour;
            LookBehaviour = lookBehaviour;
            TeleportBehaviour = teleportBehaviour;
            SelfCriterions = actorTargetability;
            TargetCriterions = target;
            Allegiance = Allegiance.Bad;

            _eventAggregator = eventAggregator;
            _eventAggregator.Subscribe(this);

            _AIComponents = new Dictionary<IAIComponent, IRequiredTargets>();
        }

        public void Update()
        {
            switch (CurrentState)
            {
                case ActorState.Patrol:
                    Patrol();
                    break;

                case ActorState.Chase:
                    Chase();
                    break;

                case ActorState.Battle:
                    Battle();
                    break;
                case ActorState.Dead:
                    break;
                default:
                    CurrentState = ActorState.Patrol;
                    break;
            }
        }

        public void AddAIComponent(IAIComponent component, IRequiredSource actionResolver)
        {
            var resolver = actionResolver.WithSource(this);
            _AIComponents.Add(component, resolver);
        }
        
        #region StateBehaviours

        private void Chase()
        {
            Speed = 5f;
            if (ChaseBehaviour.GetDistanceFromPlayer(Maiken.Position) <= MinSightDistance)
            {
                _eventAggregator.Publish(new BattleStartRequested());
                
                return;
            }
            ChaseBehaviour.Chase(Maiken.Position);
        }

        private void Patrol()
        {
            Speed = 1.5f;
            PatrolBehaviour.Patrol();
        }

        private void GoToBattlePosition()
        {
            ChaseBehaviour.Stop();
            CurrentState = ActorState.Battle;
            TeleportBehaviour.Teleport(BattlePosition);
        }

        private void Battle()
        {
            LookBehaviour.Look(CurrentTargetPosition);
            if (IsDead)
            {
                _eventAggregator.Publish(new EnemyDowned(this));
                CurrentState = ActorState.Dead;
            }

            if (_isReadyToBattle)
            {
                foreach (var keyValuePair in _AIComponents)
                {
                    keyValuePair.Key.Update(keyValuePair.Value, BattleBlackboard.Allies);
                }
            }
        }

        #endregion

        #region Events

        public void OnEvent(ResetTargetablesRequested e)
        {
            IsTargetable = false;
        }

        public void OnEvent(BattleStarted e)
        {
            GoToBattlePosition();
        }
        
        public void OnEvent(PlayerEntered e)
        {
            CurrentState = ActorState.Chase;
        }

        public void OnEvent(PlayerExited e)
        {
            CurrentState = ActorState.Patrol;
            PatrolBehaviour.SetToCurrentDestination();
        }
        
        public void OnEvent(BattleEnded e)
        {
            _isReadyToBattle = false;
            CurrentState = ActorState.Patrol;
            PatrolBehaviour.Reset();
            Revive();
        }

        public void Select()
        {
            IsSelected = true;
        }

        public void UnSelect()
        {
            IsSelected = false;
        }

        public IEnumerable<IRequiredTargets> GetActionResolvers()
        {
            return _AIComponents.Values;
        }

        public void OnEvent(PickPhaseFinished e)
        {
            _isReadyToBattle = true;
            _eventAggregator.Publish(new EnemyAdded(this));
        }

        public void OnEvent(TargetRequested e)
        {
            if (this.ValidAllegiance(e))
            {
                IsTargetable = true;
            }
        }

        #endregion
    }
}
