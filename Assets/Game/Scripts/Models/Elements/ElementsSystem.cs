﻿using System.Collections.Generic;

namespace Scripts.Models.Elements
{
    public class ElementsSystem
    {
        private readonly Dictionary<Stats, Element> _elements;

        public event ElementChangedEventHandler ElementChanged;

        public ElementsSystem(Element offense, Element defense, Element speed, Element environment)
        {
            _elements = new Dictionary<Stats, Element>();
            _elements.Add(Stats.Offense, offense);
            _elements.Add(Stats.Defense, defense);
            _elements.Add(Stats.Speed, speed);
            _elements.Add(Stats.Environment, environment);
        }

        public void ChangeStatElement(Stats stat, Element element)
        {
            _elements[stat] = element;
            OnElementChanged(new KeyValuePair<Stats, Element>(stat, element));
        }

        public float CalculateModifier(Element targetElement, Stats sourceStat)
        {
            return Compare(_elements[sourceStat], targetElement);
        }

        public float CalculateModifier(Stats targetStat, Stats sourceStat)
        {
            return Compare(_elements[sourceStat], _elements[targetStat]);
        }

        public Element Get(Stats stat)
        {
            return _elements[stat];
        }

        private float Compare(Element sourceElement, Element targetElement)
        {
            switch (targetElement)
            {
                case Element.Fire:
                    if (sourceElement == Element.Fire) return 1f;
                    if (sourceElement == Element.Water) return 0.5f;
                    if (sourceElement == Element.Wood) return 2f;
                    return 1f;
                case Element.Water:
                    if (sourceElement == Element.Fire) return 2f;
                    if (sourceElement == Element.Water) return 1f;
                    if (sourceElement == Element.Wood) return 0.5f;
                    return 1f;
                case Element.Wood:
                    if (sourceElement == Element.Fire) return 0.5f;
                    if (sourceElement == Element.Water) return 2f;
                    if (sourceElement == Element.Wood) return 1f;
                    return 1f;
                default:
                    return 1f;
            }
        }

        protected virtual void OnElementChanged(KeyValuePair<Stats, Element> element)
        {
            ElementChanged(this, element);
        }

        public delegate void ElementChangedEventHandler(object sender, KeyValuePair<Stats, Element> element);
    }
}
