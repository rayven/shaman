﻿namespace Scripts.Models.Elements
{
    public class ElementModifierCalculator
    {
        public float CalculateModifier(Element targetElement, Element sourceElement)
        {
            switch (targetElement)
            {
                    case Element.Fire:
                        if (sourceElement == Element.Fire) return 1f;
                        if (sourceElement == Element.Water) return 2f;
                        if (sourceElement == Element.Wood) return 0.5f;
                        return 1f;
                    case Element.Water:
                        if (sourceElement == Element.Fire) return 0.5f;
                        if (sourceElement == Element.Water) return 1f;
                        if (sourceElement == Element.Wood) return 2f;
                        return 1f;
                    case Element.Wood:
                        if (sourceElement == Element.Fire) return 2f;
                        if (sourceElement == Element.Water) return 0.5f;
                        if (sourceElement == Element.Wood) return 1f;
                        return 1f;
                    default:
                        return 1f;
            }
        }
    }

    public enum Element
    {
        Water,
        Fire,
        Wood,
        Stone
    }

    public enum Stats
    {
        Offense,
        Defense,
        Speed,
        Environment
    }

}
