﻿using Scripts.Models.Events;
using Scripts.Models.Resolvers;
using Scripts.Models.viewModels;
using System.Collections.Generic;
using System.Linq;
using Assets.Game.Scripts.Models.Resolvers.Builders;
using Scripts.Models.Actors;
using UnityEngine;

namespace Scripts.Models.AI
{
    public class CooldownAIComponent 
        : IAIComponent
    {
        private bool _isReady;
        private float _timeSpan;
        private float _cooldown;
        private IEventManager _eventManager;

        public CooldownAIComponent(IEventManager eventManager, float cooldown)
        {
            _cooldown = cooldown;
            _eventManager = eventManager;
        }

        public void Update(IRequiredTargets actionResolver, List<ISelectable> targets)
        {
            if(!_isReady)
            {
                ResetTimeSpan();
                _isReady = true;
            }

            if (Time.time >= _timeSpan)
            {
                var buildableActionResolver = actionResolver.WithTargets(targets);
                _eventManager.CurrentEventAggregator.Publish(
                    new ActionResolverPushed(new List<IActionResolver>() { buildableActionResolver.Build() })
                );
                ResetTimeSpan();
            }
        }

        private List<ISelectable> ChooseTarget(List<ISelectable> _targets)
        {
            var firstTargetAsActor = _targets.Last() as Actor;
            if (firstTargetAsActor.IsDead)
            {
                return _targets.GetRange(0,1);
            }
            return _targets.GetRange(1,1);
        }

        private void ResetTimeSpan()
        {
            _timeSpan = Time.time + _cooldown + (Random.value*_cooldown);
        }
    }
}
