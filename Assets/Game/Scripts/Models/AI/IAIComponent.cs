﻿using Scripts.Models.Resolvers;
using Scripts.Models.viewModels;
using System.Collections.Generic;
using Assets.Game.Scripts.Models.Resolvers.Builders;

namespace Scripts.Models.AI
{
    public interface IAIComponent
    {
        void Update(IRequiredTargets actionResolver, List<ISelectable> targets);
    }
}
