﻿namespace Scripts.Models.Events
{
    public interface IEventManager
    {
        IEventAggregator CurrentEventAggregator { get; set; }
    }

    public class EventManager : IEventManager, ISubscriber<EventAggregatorChanged>
    {
        public IEventAggregator CurrentEventAggregator { get; set; }

        public void OnEvent(EventAggregatorChanged e)
        {
            CurrentEventAggregator = e.NewEventAggregator;
        }
    }
}
