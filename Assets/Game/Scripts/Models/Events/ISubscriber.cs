﻿namespace Scripts.Models.Events
{
    public interface ISubscriber<TEventType>
    {
        void OnEvent(TEventType e);
    }
}
