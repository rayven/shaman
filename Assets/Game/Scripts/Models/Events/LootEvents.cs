﻿using System.Collections.Generic;
using Scripts.Services.Loot;

namespace Scripts.Models.Events
{
    public class ShowLootRequested
    {
        public List<ILootable> Lootables { get; set; }

        public ShowLootRequested(List<ILootable> lootables)
        {
            Lootables = lootables;
        }
    }
    public class LootCalculationRequested
    {
        public Dictionary<ILootable, float> LootablesWithPercentageChances { get; set; }

        public LootCalculationRequested(Dictionary<ILootable, float> lootables)
        {
            LootablesWithPercentageChances = lootables;
        }
    }
    public class AddLootableToBagRequested
    {
        public ILootable Lootable { get; set; }

        public AddLootableToBagRequested(ILootable lootable)
        {
            Lootable = lootable;
        }
    }
    public class AddLootableRejected { }
    public class LootCanvasRequested
    {
        public List<ILootable> Lootables { get; set; }

        public LootCanvasRequested(List<ILootable> lootables)
        {
            Lootables = lootables;
        }
    }
}
