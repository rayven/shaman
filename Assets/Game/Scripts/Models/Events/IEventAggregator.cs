﻿using System;
using System.Collections.Generic;
using Scripts.Models.Actors;
using Scripts.Models.Cards;
using Scripts.Models.Resolvers;
using Scripts.Models.viewModels;
using Scripts.Services.Battles;
using Scripts.Services.Spells;
using Scripts.Utils.Enums;
using UnityEngine;
using Scripts.Models.Elements;
using Scripts.Models.Targetabilities;

namespace Scripts.Models.Events
{
    public interface IEventAggregator
    {
        void Publish<TEventType>(TEventType eventToPublish);
        void Subscribe(object subscriber);
        void Unsubscribe(object subscriber);
    }

    public class BattlegroundRemoved {}

    public class BattleServiceChanged
    {
        public BattleService BattleService { get; set; }

        public BattleServiceChanged(BattleService battleService)
        {
            BattleService = battleService;
        }
    }

    public class BattleEnded {}
    public class BattleStarted { }
    public class BattleStartRequested {}
    
    public class PlayerEntered
    {
        public Element Environment { get; set; }

        public PlayerEntered(Element environment)
        {
            Environment = environment;
        }
    }

    public class PlayerExited {}
    public class PlayerWon {}
    public class PlayerLost {}

    public class Clicked {}

    public class CardPlayed
    {
        public Card Card { get; set; }

        public CardPlayed(Card card)
        {
            Card = card;
        }
    }

    public class PickCardRequested
    {
        public Ally Ally { get; set; }

        public PickCardRequested(Ally ally)
        {
            Ally = ally;
        }
    }
    public class HandIsFull { }
    public class RespawnRequested { }
    public class RespawnCanvasRequested { }
    
    public class PickPhaseFinished { }

    public class SpawnTotemBasesRequested
    {
        public Battleground Battleground { get; set; }

        public SpawnTotemBasesRequested(Battleground battleground)
        {
            Battleground = battleground;
        }
    }

    public class CardPicked
    {
        public Card Card { get; set; }

        public CardPicked(Card card)
        {
            Card = card;
        }
    }
    
    public class EnemyDowned
    {
        public ISelectable Enemy { get; set; }

        public EnemyDowned(ISelectable enemy)
        {
            Enemy = enemy;
        }
    }

    public class AllyDowned
    {
        public ISelectable Ally { get; set; }

        public AllyDowned(ISelectable ally)
        {
            Ally = ally;
        }
    }

    public class EnemyAdded
    {
        public ISelectable Enemy { get; set; }

        public EnemyAdded(ISelectable enemy)
        {
            Enemy = enemy;
        }
    }

    public class AllyAdded
    {
        public ISelectable Ally { get; set; }

        public AllyAdded(ISelectable ally)
        {
            Ally = ally;
        }
    }

    public class Attacked
    {
        public ISelectable Source { get; set; }

        public Attacked(ISelectable source)
        {
            Source = source;
        }
    }

    public class ActionResolverPushed
    {
        public List<IActionResolver> ActionResolvers { get; set; }

        public ActionResolverPushed(IEnumerable<IActionResolver> actionResolvers)
        {
            ActionResolvers = new List<IActionResolver>(actionResolvers);
        }
    }

    public class SelectRequested
    {
        public ISelectable RequestedSelectable { get; set; }

        public SelectRequested(ISelectable selectable)
        {
            RequestedSelectable = selectable;
        }
    }

    public class AllyPickCardRequested
    {
        public ISelectable Selectable { get; set; }
        public Enums.AllyType Ally { get; set; }

        public AllyPickCardRequested(ISelectable selectable, Enums.AllyType type)
        {
            Selectable = selectable;
            Ally = type;
        }
    }

    public class ResetTargetablesRequested { }
    public class ResetSelectionRequested { }
    public class TargetRequested
    {
        public TargettingCriterions TargetCriterions { get; set; }
        public Allegiance SourceAllegiance { get; set; }

        public TargetRequested(TargettingCriterions targetCharacteristics, Allegiance sourceAllegiance)
        {
            TargetCriterions = targetCharacteristics;
            SourceAllegiance = sourceAllegiance;
        }
    }

    public class SpellCasted
    {
        public FxType Type { get; set; }
        public Vector3 Source { get; set; }
        public Vector3 Destination { get; set; }
        public Action Callback { get; set; }

        public SpellCasted(FxType type, Vector3 source, Vector3 destination, Action callback)
        {
            Type = type;
            Source = source;
            Destination = destination;
            Callback = callback;
        }
    }

    public class EventAggregatorChanged
    {
        public IEventAggregator NewEventAggregator { get; set; }

        public EventAggregatorChanged(IEventAggregator eventAggregator)
        {
            NewEventAggregator = eventAggregator;
        }
    }
}
