﻿using System.Collections.Generic;
using Scripts.Models.viewModels;

namespace Scripts.Models.Resolvers
{
    public interface IActionResolver
    {
        bool IsFinished { get; set; }

        void Resolve();
        void SetTargets(IEnumerable<ISelectable> targets);
        void SetSource(ISelectable source);
    }
}
