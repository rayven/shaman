﻿using System.Collections.Generic;
using System.Linq;
using Scripts.Models.Actors;
using Scripts.Models.Events;
using Scripts.Models.viewModels;

namespace Scripts.Models.Resolvers
{
    public class DownAllEnemiesVictoryActionResolver : IActionResolver, 
        ISubscriber<EnemyDowned>,
        ISubscriber<AllyDowned>,
        ISubscriber<AllyAdded>,
        ISubscriber<EnemyAdded>
    {
        public bool IsFinished { get; set; }

        private IEventManager _eventManager;
        private List<ISelectable> _allies;
        private List<ISelectable> _enemies;
        
        public DownAllEnemiesVictoryActionResolver(IEventManager eventManager)
        {
            _eventManager = eventManager;
            _allies = new List<ISelectable>();
            _enemies = new List<ISelectable>();
        }

        public void Resolve()
        {}

        private void CheckBattleEnd()
        {
            if (!_allies.Any())
            {
                _eventManager.CurrentEventAggregator.Publish(new PlayerLost());
                return;
            }

            if (!_enemies.Any())
            {
                _eventManager.CurrentEventAggregator.Publish(new PlayerWon());
            }
        }

        public void SetTargets(IEnumerable<ISelectable> targets)
        {}

        public void SetSource(ISelectable source)
        {}

        public void OnEvent(EnemyAdded e)
        {
            _enemies.Add(e.Enemy);
        }

        public void OnEvent(AllyAdded e)
        {
            _allies.Add(e.Ally);
        }

        public void OnEvent(EnemyDowned e)
        {
            var enemy = e.Enemy;
            if (_enemies.Contains(enemy))
            {
                _enemies.Remove(enemy);
                CheckBattleEnd();
            }
        }

        public void OnEvent(AllyDowned e)
        {
            var ally = e.Ally;
            if (_allies.Contains(ally))
            {
                _allies.Remove(ally);
                CheckBattleEnd();
            }
        }

        public void SetCaster(Actor caster)
        {}
    }
}
