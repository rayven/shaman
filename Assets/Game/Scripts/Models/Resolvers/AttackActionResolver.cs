﻿using System.Collections.Generic;
using System.Linq;
using Scripts.Models.Actors;
using Scripts.Models.viewModels;
using UnityEngine;
using Scripts.Models.Elements;
using Scripts.Models.Events;
using Scripts.Models.Resolvers.CardsResolvers;

namespace Scripts.Models.Resolvers
{
    public class AttackActionResolver 
        : ActionResolver
        , IActionResolver
    {
        private readonly float _cooldownTime;
        
        public AttackActionResolver(float cooldownTime, IEventManager eventManager, Element element, bool mustWaitCasting)
            : base(eventManager, element, mustWaitCasting)
        {
            IsFinished = false;
            _cooldownTime = cooldownTime;
            Action = Attack;
        }

        public void Attack()
        {
            if (Source.IsCasting) return;

            IsFinished = true;
            Source.Attack(_cooldownTime, ResolveDamage);
        }

        private void ResolveDamage()
        {
            Targets.ForEach(t => t.ApplyToHealth(-Source.Damage, Source.ElementsSystem.Get(Stats.Offense)));
            Targets.ForEach(t => Debug.Log(t.CurrentHealth + "/" + t.MaxHealth));
        }

    }
}
