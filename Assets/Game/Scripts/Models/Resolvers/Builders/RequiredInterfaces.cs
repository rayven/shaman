﻿using System.Collections.Generic;
using Scripts.Models.Resolvers;
using Scripts.Models.viewModels;

namespace Assets.Game.Scripts.Models.Resolvers.Builders
{
    public interface IRequiredSource
    {
        IRequiredTargets WithSource(ISelectable source);
    }

    public interface IRequiredTargets
    {
        IActionResolverBuildable WithTargets(IEnumerable<ISelectable> targets);
    }

    public interface IActionResolverBuildable
    {
        IActionResolver Build();
    }
}
