﻿using System.Collections.Generic;
using Scripts.Models.Actors;
using Scripts.Models.Cards;
using Scripts.Models.Elements;
using Scripts.Models.Events;
using Scripts.Models.Resolvers;
using Scripts.Models.Resolvers.CardsResolvers;
using Scripts.Models.viewModels;

namespace Assets.Game.Scripts.Models.Resolvers.Builders
{
    public class ActionResolverBuilder 
        : IActionResolverBuilder
        , IRequiredSource
        , IRequiredTargets
        , IActionResolverBuildable
    {
        private ActionResolver _actionResolver;

        public ActionResolverBuilder()
        {}

        public ActionResolverBuilder(ActionResolver actionResolver)
        {
            _actionResolver = actionResolver;
        }
        
        public IRequiredSource ActionResolver(ActionResolver actionResolver)
        {
            return new ActionResolverBuilder(actionResolver);
        }

        public IRequiredTargets WithSource(ISelectable source)
        {
            _actionResolver.SetSource(source);

            return new ActionResolverBuilder(_actionResolver);
        }

        public IActionResolverBuildable WithTargets(IEnumerable<ISelectable> targets)
        {
            _actionResolver.SetTargets(targets);

            return new ActionResolverBuilder(_actionResolver);
        }

        public IActionResolver Build()
        {
            return _actionResolver;
        }
    }

    public interface IActionResolverBuilder
    {
        IRequiredSource ActionResolver(ActionResolver card);
    }

    public interface IRequiredCaster
    {
        IRequiredSource WithCaster(Actor source);
    }
}
