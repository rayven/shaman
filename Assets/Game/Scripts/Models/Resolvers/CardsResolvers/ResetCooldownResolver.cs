﻿using Scripts.Models.Actors;
using Scripts.Models.Elements;
using Scripts.Models.Events;
using Scripts.Services.Spells;
using Scripts.Utils.Constants;

namespace Scripts.Models.Resolvers.CardsResolvers
{
    public class ResetCooldownResolver : ActionResolver
    {
        private IVFXService _vfxService;

        public ResetCooldownResolver(IVFXService vfxService, Element element, IEventManager eventManager) 
            : base(eventManager, element, false)
        {
            _vfxService = vfxService;
            Action = ResetCooldown;
        }

        private void ResetCooldown()
        {
            Targets.ForEach(t => t.ResetCooldown());
            Targets.ForEach(t => ShowVisualEffect(t));
        }

        private void ShowVisualEffect(Actor actor)
        {
            _vfxService.Instantiate(ParticlePaths.CooldownReset, actor.CooldownIndicatorPosition);
        }
    }
}
