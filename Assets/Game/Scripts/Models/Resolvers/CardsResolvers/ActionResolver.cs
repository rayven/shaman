﻿using System;
using System.Collections.Generic;
using System.Linq;
using Scripts.Models.Actors;
using Scripts.Models.Events;
using Scripts.Models.viewModels;
using Scripts.Models.Elements;

namespace Scripts.Models.Resolvers.CardsResolvers
{
    public class ActionResolver 
        : IActionResolver
        , ISubscriber<RespawnCanvasRequested>
        , ISubscriber<BattleEnded>
    {
        protected IEventManager EventManager { get; set; }

        protected Action Action;

        protected Actor Source;
        protected List<Actor> Targets;
        
        protected Element Element;

        public bool IsCanceled { get; set; }
        public bool MustWaitCasting { get; set; }
        public bool IsFinished { get; set; }

        public ActionResolver(IEventManager eventManager, Element element, bool mustWaitCasting)
        {
            EventManager = eventManager;
            Element = element;
            MustWaitCasting = mustWaitCasting;
        }

        public void Resolve()
        {
            if (MustWaitCasting && Source.IsCasting) return;
            IsFinished = true;

            Action();
        }
        
        public void SetTargets(IEnumerable<ISelectable> targets)
        {
            Targets = targets.Select(t => (Actor)t).ToList();
        }

        public void SetSource(ISelectable source)
        {
            Source = (Actor)source;
        }
        
        public void OnEvent(RespawnCanvasRequested e)
        {
            IsCanceled = true;
        }

        public void OnEvent(BattleEnded e)
        {
            IsCanceled = true;
        }
    }
}
