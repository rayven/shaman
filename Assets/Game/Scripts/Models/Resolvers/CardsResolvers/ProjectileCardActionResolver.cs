﻿using Scripts.Models.Events;
using Scripts.Services.Spells;
using System.Collections.Generic;
using Scripts.Models.Actors;
using UnityEngine;
using Scripts.Models.Elements;

namespace Scripts.Models.Resolvers.CardsResolvers
{
    public class ProjectileSpellActionResolver: ActionResolver
    {
        private readonly long _damage;
        private IVFXService _vfxService;
        private string _projectileParticle;
        private string _callbackParticle;

        public ProjectileSpellActionResolver(
            IVFXService vfxService, 
            long damage, 
            Element element, 
            IEventManager eventManager, 
            string projectileParticle, 
            string callbackParticle)
            : base (eventManager, element, true)
        {
            _vfxService = vfxService;
            _damage = damage;
            _projectileParticle = projectileParticle;
            _callbackParticle = callbackParticle;
            Action = () =>
            {
                StartAnimation();
            };
        }
         
        private void StartAnimation()
        {
            if (IsCanceled) return;
            Source.Cast(CastSpell);
        }

        private void CastSpell()
        {
            if (IsCanceled) return;
            foreach (var t in Targets)
            {
                var target = t;
                _vfxService
                    .Instantiate(_projectileParticle, Source.HandsPosition)
                    .AsProjectileWithCallback(target.HandsPosition, () =>
                    {
                        _vfxService.Instantiate(_callbackParticle, target.HandsPosition);
                        ResolveDamage(new List<Actor>() { target });
                    });
            }
        }

        private void ResolveDamage(List<Actor> targets)
        {
            if (IsCanceled) return;
            Source.IsCasting = false;
            targets.ForEach(target =>
            {
                target.ApplyToHealth(-_damage, Element);
                Debug.Log(target.CurrentHealth + "/" + target.MaxHealth);
            });
        }
    }
}
