﻿using Scripts.Models.Elements;
using Scripts.Models.Entities;
using Scripts.Models.Events;
using Scripts.Services.Spells;
using Scripts.Utils.Constants;
using UnityEngine;

namespace Scripts.Models.Resolvers.CardsResolvers
{
    public class HealActionResolver : ActionResolver
    {
        private readonly long _healthAmount;
        private IVFXService _vfxService;

        public HealActionResolver(IVFXService vfxService, long healthAmount, Element element, IEventManager eventManager)
            :base(eventManager, element, true)
        {
            _vfxService = vfxService;
            _healthAmount = healthAmount;
            Action = () =>
            {
                StartAnimation();
            };
        }

        private void StartAnimation()
        {
            if (IsCanceled) return;
            Source.Cast(() => {});
            foreach (var target in Targets)
            {
                _vfxService.Instantiate(ParticlePaths.LeavesCaster, Source.Position)
                    .WithCallback(() =>
                    {
                        _vfxService.Instantiate(ParticlePaths.LeavesReceive, target.Position);
                        ResolveHeal();
                    });
            }
        }

        private void ResolveHeal()
        {
            if (IsCanceled) return;
            Source.IsCasting = false;
            Targets.ForEach(be =>
            {
                var target = be;
                target.ApplyToHealth(_healthAmount, Element);
                Debug.Log(target.CurrentHealth + "/" + target.MaxHealth);
            });
        }
    }
}
