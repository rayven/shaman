﻿using Scripts.Models.Elements;
using Scripts.Models.Events;

namespace Scripts.Models.Resolvers.CardsResolvers
{
    public class ChangeTotemPartActionResolver : ActionResolver
    {
        public ChangeTotemPartActionResolver(IEventManager eventManager, Element element, bool mustWaitCasting) 
            : base(eventManager, element, mustWaitCasting)
        {
            Action = () =>
            {
                Targets.ForEach(x => x.ElementsSystem.ChangeStatElement(Stats.Defense, element));
            };
        }
    }
}
