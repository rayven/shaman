﻿using System.Collections.Generic;
using Assets.dotNetExtensions;
using Assets.Game.Scripts.Models.Resolvers.Builders;
using Scripts.Models.Events;
using Scripts.Models.Targetabilities;

namespace Scripts.Models.viewModels
{
    public interface ISelectable
    {
        TargettingCriterions SelfCriterions { get; }
        TargettingCriterions TargetCriterions { get; }
        Allegiance Allegiance { get; }

        bool IsSelected { get; set; }
        bool IsTargetable { get; set; }
        void Select();
        void UnSelect();

        IEnumerable<IRequiredTargets> GetActionResolvers();
    }

    public static class SelectableExtensions
    {
        public static bool ValidAllegiance(this ISelectable selectable, TargetRequested targetRequest)
        {
            //todo: Gerer les neutres
            var sameAllegianceWanted = targetRequest.TargetCriterions.HasFlag(TargettingCriterions.Allies) ? true : false;
            var sourceIsGood = targetRequest.SourceAllegiance.HasFlag(Allegiance.Good);
            var targetIsGood = selectable.Allegiance == Allegiance.Good;

            var sourceAndTargetSameAllegiance = sourceIsGood == targetIsGood;

            return sameAllegianceWanted == sourceAndTargetSameAllegiance;
        }
    }
}
