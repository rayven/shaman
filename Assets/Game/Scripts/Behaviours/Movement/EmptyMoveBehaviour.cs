﻿using System;
using UnityEngine;

namespace Scripts.Behaviours.Movement
{
    public class EmptyMoveBehaviour : MonoBehaviour, IMoveBehaviour
    {
        private CharacterController _controller;
        private float _gravity = 14f;

        public void Awake()
        {
            _controller = GetComponent<CharacterController>();
        }

        public void Move(Vector3 direction, float speed)
        {
            _controller.Move(new Vector3(0, -_gravity, 0));
        }
    }
}
