﻿using Scripts.Models.Actors;
using Scripts.Utils.Constants;
using strange.extensions.mediation.impl;
using Scripts.Managers.Game;
using Scripts.Managers.Inputs;
using UnityEngine;

namespace Scripts.Behaviours.Movement
{
    public class PlayerMoveBehaviour : View, IMoveBehaviour
    {
        [Inject("Maiken")]
        public Ally Maiken { get; set; }
        [Inject(GameState.SideScroller)]
        public IInputManager SSInputManager { get; set; }

        private CharacterController _controller;
        private Animator _animator;
        private float _gravity = 14.0f;
        private float _velocity;
        private Vector3 _lastPosition;

        private const float JUMP_POWER = 12.0f;

        protected override void Awake()
        {
            _controller = GetComponent<CharacterController>();
            _animator = GetComponent<Animator>();
            _lastPosition = new Vector3();
        }
        
        public void Move(Vector3 direction, float speed)
        {
            LookForward(direction);
            
            if (IsGrounded())
            {
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    _animator.SetTrigger(AnimatorParameters.Jump);
                    _velocity = JUMP_POWER;
                }
            }
            _velocity -= _gravity * Time.deltaTime;
            direction.x *= speed;
            direction.y = _velocity;
            direction.z *= speed;
            _controller.Move(direction * Time.deltaTime);
            _lastPosition = transform.position;
        }

        private bool IsGrounded()
        {
            return Physics.Raycast(transform.position, -transform.up, 100f);
        }
        
        private bool IsHighJumpState()
        {
            return _animator.GetCurrentAnimatorStateInfo(0).IsName(AnimatorParameters.HighJump);
        }

        private void LookForward(Vector3 direction)
        {
            var normalized = transform.position + (direction*100);
            normalized.y = transform.position.y;
            transform.LookAt(normalized);
        }
    } 
}
