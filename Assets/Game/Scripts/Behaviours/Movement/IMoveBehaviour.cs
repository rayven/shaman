﻿using UnityEngine;

namespace Scripts.Behaviours.Movement
{
    public interface IMoveBehaviour
    {
        void Move(Vector3 direction, float speed);
    }
}
