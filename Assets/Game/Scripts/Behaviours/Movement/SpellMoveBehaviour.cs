﻿using UnityEngine;

namespace Scripts.Behaviours.Movement
{
    public class SpellMoveBehaviour : MonoBehaviour
        , IMoveBehaviour
    {
        public void Move(Vector3 direction, float speed)
        {
            transform.LookAt(direction);
            transform.Translate(Vector3.forward * speed * Time.deltaTime);
        }
    }
}
