﻿using Scripts.Utils.Constants;
using UnityEngine;

namespace Scripts.Behaviours.Patrol
{
    public class EnemyPatrolBehaviour : MonoBehaviour, IPatrolBehaviour
    {
        public Transform[] WayPoints;

        private NavMeshAgent _agent;
        private NavMeshPath _path;
        private Animator _animator;
        private long _nextWayPoint;
        private float _timespan;
        private bool _isWaiting;

        public void Start()
        {
            _agent = GetComponent<NavMeshAgent>();
            _path = new NavMeshPath();
            _animator = GetComponent<Animator>();
        }

        public void Reset()
        {
            _nextWayPoint = 0;
            _agent.destination = WayPoints[_nextWayPoint].position;
        }

        public void Patrol()
        {
            if (WayPoints.Length > 0)
            {
                _agent.Resume();
                
                _agent.CalculatePath(_agent.destination, _path);
                if (_path.status == NavMeshPathStatus.PathPartial || _agent.remainingDistance <= _agent.stoppingDistance)
                {
                    if (_isWaiting)
                    {
                        if (Time.time >= _timespan)
                        {
                            _isWaiting = false;
                            SetToNextDestination();
                            _animator.SetBool(AnimatorParameters.IsPatroling, true);
                            _animator.SetBool(AnimatorParameters.IsIdle, false);
                        }
                    }
                    else
                    {
                        _isWaiting = true;
                        _timespan = Time.time + Random.Range(3, 5);
                        _animator.SetBool(AnimatorParameters.IsPatroling, false);
                        _animator.SetBool(AnimatorParameters.IsIdle, true);
                    }
                }
            }
        }

        public void SetToNextDestination()
        {
            _nextWayPoint = (_nextWayPoint + 1)%WayPoints.Length;
            _agent.SetDestination(WayPoints[_nextWayPoint].position);
        }

        public void SetToCurrentDestination()
        {
            _agent.SetDestination(WayPoints[_nextWayPoint].position);
        }
    }
}
