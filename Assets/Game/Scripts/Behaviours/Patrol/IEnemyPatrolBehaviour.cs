﻿using UnityEngine;

namespace Scripts.Behaviours.Patrol
{
    public interface IPatrolBehaviour
    {
        void Patrol();
        void Reset();
        void SetToNextDestination();
        void SetToCurrentDestination();
    }
}
