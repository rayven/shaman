﻿using UnityEngine;

namespace Scripts.Behaviours.Look
{
    public class PlayerLookBehaviour : MonoBehaviour, ILookBehaviour
    {
        public void Look(Vector3 position)
        {
            transform.LookAt(position);
            transform.rotation = Quaternion.Euler(0, 90, 0);
        }
    }
}
