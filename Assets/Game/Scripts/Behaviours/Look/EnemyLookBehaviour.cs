﻿using UnityEngine;

namespace Scripts.Behaviours.Look
{
    public class EnemyLookBehaviour : MonoBehaviour, ILookBehaviour
    {
        private NavMeshAgent _agent;

        public void Start()
        {
            _agent = GetComponent<NavMeshAgent>();
        }

        public void Look(Vector3 position)
        {
            _agent.transform.LookAt(position, Vector3.up);
        }
    }
}
