﻿using UnityEngine;

namespace Scripts.Behaviours.Look
{
    public interface ILookBehaviour
    {
        void Look(Vector3 position);
    }
}
