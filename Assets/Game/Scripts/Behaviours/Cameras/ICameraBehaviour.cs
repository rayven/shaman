﻿using Scripts.Models.Actors;

namespace Scripts.Behaviours.Cameras
{
    public interface ICameraBehaviour
    {
        void Init(Ally maiken);
        void Follow();
    }

    public enum CameraMode
    {
        SideExtremeLongShot,
        OverShoulderExtremeLongShot,
        BattleShot
    }
}
