﻿using Scripts.Models.Actors;
using UnityEngine;

namespace Scripts.Behaviours.Cameras
{
    public class BattleCameraBehaviour : MonoBehaviour, ICameraBehaviour
    {
        private Ally _player;
        public float SmoothSpeed;
        public float XOffSet = 7;
        public float YOffSet = 5;
        public float ZOffSet = 10;
        public float XRotationInDegrees = 30;
        public void Init(Ally player)
        {
            _player = player;
        }

        public void Follow()
        {
            var cameraX = _player.Position.x + XOffSet;
            var cameraY = _player.Position.y + YOffSet;
            var cameraZ = _player.Position.z - ZOffSet;
            var cameraPosition = new Vector3(cameraX, cameraY, cameraZ);

            transform.rotation = Quaternion.Euler(XRotationInDegrees, 0, 0);
            transform.position = Vector3.Lerp(transform.position, cameraPosition, SmoothSpeed * Time.deltaTime);
        }
    }
}
