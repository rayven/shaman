﻿using UnityEngine;

namespace Scripts.Behaviours.Cameras
{
    public class SwitchHealthBarSizeBehaviour : MonoBehaviour
    {
        public GameObject SmallBar;
        public GameObject BigBar;

        public void Start()
        {
            BigBar.SetActive(false);
            SmallBar.SetActive(true);
        }

        public void ActivateDetailBar()
        {
            SmallBar.SetActive(false);
            BigBar.SetActive(true);
        }

        public void Reset()
        {
            BigBar.SetActive(false);
            SmallBar.SetActive(true);
        }
    }
}
