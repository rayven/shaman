﻿using Scripts.Models;
using Scripts.Models.Actors;
using UnityEngine;

namespace Scripts.Behaviours.Cameras
{
    public class SideExtremeLongShotCameraBehaviour : MonoBehaviour, ICameraBehaviour
    {
        private Ally _player;
        public float SmoothSpeed = 2f;
        public float XOffSet;
        public float YOffSet;
        public float ZOffSet;
        public float XRotation;
        public float YRotation;

        public void Init(Ally player)
        {
            _player = player;
        }
        
        public void Follow()
        {
            var cameraX = _player.Position.x + XOffSet;
            var cameraY = _player.Position.y + YOffSet;
            var cameraZ = _player.Position.z - ZOffSet;
            var cameraPosition = new Vector3(cameraX, cameraY, cameraZ);

            transform.rotation = Quaternion.Euler(XRotation, YRotation, 0);
            transform.position = Vector3.Lerp(transform.position, cameraPosition, SmoothSpeed * Time.deltaTime);
        }
    }
}
