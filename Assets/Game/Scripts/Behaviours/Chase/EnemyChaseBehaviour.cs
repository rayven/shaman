﻿using UnityEngine;

namespace Scripts.Behaviours.Chase
{
    public class EnemyChaseBehaviour : MonoBehaviour, IChaseBehaviour
    {
        public NavMeshAgent Agent { get; set; }

        public void Start()
        {
            Agent = GetComponent<NavMeshAgent>();
        }

        public float GetDistanceFromPlayer(Vector3 playerPosition)
        {
            var normalizedPlayerPosition = new Vector3(playerPosition.x, 0, playerPosition.z);
            return Vector3.Distance(
                transform.position,
                normalizedPlayerPosition);
        }

        public void Chase(Vector3 position)
        {
            Agent.destination = position;
            Agent.Resume();
        }

        public void Stop()
        {
            Agent.Stop();
        }
    }
}
