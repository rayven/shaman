﻿using UnityEngine;

namespace Scripts.Behaviours.Chase
{
    public interface IChaseBehaviour
    {
        void Chase(Vector3 position);
        void Stop();
        float GetDistanceFromPlayer(Vector3 playerPosition);
    }
}
