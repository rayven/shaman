﻿using System;

namespace Scripts.Behaviours.Attack
{
    public interface IAttackBehaviour
    {
        void Attack(Action callback);
        void Cast(Action callback);
    }
}
