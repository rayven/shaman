﻿using Scripts.Behaviours.Attack;
using System;
using Scripts.Utils.Constants;
using UnityEngine;

namespace Scripts.Behaviours.Attack
{
    public class EnemyAttackBehaviour : MonoBehaviour, IAttackBehaviour
    {
        private Action _callback;
        private Animator _animator;

        public void Start()
        {
            _animator = GetComponent<Animator>();
        }

        public void Update()
        {
            if(_callback != null)
            {
                _callback();
                _callback = null;
            }
        }

        public void Attack(Action callback)
        {
            _callback = callback;
        }

        public void Cast(Action callback)
        {
            _animator.SetTrigger(AnimatorParameters.Attack);
            _callback = callback;
        }
    }
}
