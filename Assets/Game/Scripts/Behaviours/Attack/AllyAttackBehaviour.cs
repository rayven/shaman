﻿using System;
using Scripts.Utils.Constants;
using UnityEngine;

namespace Scripts.Behaviours.Attack
{
    public class AllyAttackBehaviour : MonoBehaviour, IAttackBehaviour
    {
        private Animator _animator;
        private Action _callback;
        private bool _isCallbackReady;
        private bool _isAnimating;
        private string _currentState;

        public void Start()
        {
            _animator = GetComponent<Animator>();
        }

        public void Update()
        {
            var state = _animator.GetCurrentAnimatorStateInfo(0).IsName(_currentState);
            if (state && _isAnimating)
            {
                _isCallbackReady = true;
                _isAnimating = false;
            }
            else if (!state && _isCallbackReady)
            {
                _callback();
                _isCallbackReady = false;
            }
        }
        
        public void Attack(Action callback)
        {
            _animator.SetTrigger(AnimatorParameters.Attack);
            _currentState = AnimatorState.Attack;
            _isAnimating = true;
            _callback = callback;
        }

        public void Cast(Action callback)
        {
            _animator.SetTrigger(AnimatorParameters.Cast);
            _currentState = AnimatorState.Spell;
            _isAnimating = true;
            _callback = callback;
        }
    }
}
