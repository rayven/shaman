﻿using UnityEngine;

namespace Scripts.Behaviours.Teleport
{
    public class TeleportBehaviour : MonoBehaviour, ITeleportBehaviour
    {
        private Transform _transform;
        
        public void Start()
        {
            _transform = GetComponent<Transform>();
        }

        public void Teleport(Vector3 position)
        {
            _transform.position = position;
        }
    }
}
