﻿using UnityEngine;

namespace Scripts.Behaviours.Teleport
{
    public class PlayerTeleportBehaviour : MonoBehaviour, ITeleportBehaviour
    {
        private CharacterController _controller;
        private Collider _collider;

        public void Start()
        {
            _controller = GetComponent<CharacterController>();
            _collider = _controller.GetComponent<Collider>();
        }

        public void Teleport(Vector3 position)
        {
            _controller.transform.position = position;
        }
    }
}
