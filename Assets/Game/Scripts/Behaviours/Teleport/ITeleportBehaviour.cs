﻿using UnityEngine;

namespace Scripts.Behaviours.Teleport
{
    public interface ITeleportBehaviour
    {
        void Teleport(Vector3 position);
    }
}
