﻿using UnityEngine;

namespace Scripts.Behaviours.Navigation
{
    public interface INavigationBehaviour
    {
        void NavigateTo(Vector3 position, float speed);
    }
}
