﻿using UnityEngine;

namespace Scripts.Behaviours.Navigation
{
    public class PlayerNavigationBehaviour : MonoBehaviour, INavigationBehaviour
    {
        public void NavigateTo(Vector3 position, float speed)
        {
            var step = speed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(
            transform.position, position, step);
        }
    }
}
