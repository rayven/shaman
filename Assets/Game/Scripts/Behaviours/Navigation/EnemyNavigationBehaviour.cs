﻿using UnityEngine;

namespace Scripts.Behaviours.Navigation
{
    public class EnemyNavigationBehaviour : MonoBehaviour, INavigationBehaviour
    {
        public NavMeshAgent Agent { get; set; }

        public void NavigateTo(Vector3 position, float speed)
        {
            Agent.speed = speed;
            Agent.destination = position;
            Agent.Resume();
        }
    }
}
