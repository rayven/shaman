﻿using Scripts.Models.Entities;
using Scripts.Models.Events;
using strange.extensions.mediation.impl;
using Scripts.Models.Actors;

namespace Assets.Game.Scripts.Behaviours.Spells
{
    public class CancelOnTargetDownedBehaviour : View
        , ISubscriber<AllyDowned>
        , ISubscriber<EnemyDowned>
    {
        [Inject]
        public IEventManager EventManager { get; set; }
        public Actor Target { get; set; }

        private bool _isNotSubscribed = true;

        public void Update()
        {
            if (_isNotSubscribed)
            {
                EventManager.CurrentEventAggregator.Subscribe(this);
                _isNotSubscribed = false;
            }
        }

        public void OnEvent(EnemyDowned e)
        {
            Destroy(e.Enemy as Actor);
        }

        public void OnEvent(AllyDowned e)
        {
            Destroy(e.Ally as Actor);
        }

        private void Destroy(Actor actor)
        {
            if(Target == actor)
            {
                
            }
        }
    }
}
