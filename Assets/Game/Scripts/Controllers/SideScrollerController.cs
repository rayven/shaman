﻿using Scripts.Managers.Game;
using Scripts.Managers.Inputs;
using Scripts.Models.Actors;

namespace Scripts.Controllers
{
    public class SideScrollerController : IController
    {
        [Inject(GameState.SideScroller)]
        public IInputManager InputManager { get; set; }
        
        public void Update()
        {
            
        }

        public void FixedUpdate()
        {
            var commands = InputManager.Handle();
            if (commands != null && commands.Count != 0)
            {
                foreach (var command in commands)
                {
                    command.Execute();
                }
            }
        }
    }
}
