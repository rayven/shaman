﻿using Scripts.Managers.Inputs;

namespace Scripts.Controllers
{
    public interface IController
    {
        void Update();
        void FixedUpdate();
    }
}
