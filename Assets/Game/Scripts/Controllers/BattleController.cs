﻿using System.Collections.Generic;
using System.Linq;
using Scripts.Managers.Game;
using Scripts.Managers.Inputs;
using Scripts.Models.Events;
using Scripts.Services.Battles;
using Scripts.Services.Commands;

namespace Scripts.Controllers
{
    public class BattleController 
        : IController
        , ISubscriber<BattleServiceChanged>
    {
        [Inject(GameState.Battle)]
        public IInputManager InputManager { get; set; }
        private IBattleService _battleService;
        private List<ICommand> _commands;

        public void Update()
        {
            if (_commands != null && _commands.Count != 0)
            {
                _commands.ForEach(command => command.Execute());
            }
            _battleService.Update();
        }

        public void FixedUpdate()
        {
            _commands = InputManager.Handle();
        }

        public void OnEvent(BattleServiceChanged e)
        {
            _battleService = e.BattleService;
        }
    }
}
