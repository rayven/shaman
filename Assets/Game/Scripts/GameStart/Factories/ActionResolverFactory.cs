﻿using Assets.Game.Scripts.Models.Resolvers.Builders;
using Scripts.Models.Elements;
using Scripts.Models.Events;
using Scripts.Models.Resolvers;
using Scripts.Models.Resolvers.CardsResolvers;
using Scripts.Services.Spells;
using Scripts.Utils.Constants;

namespace Scripts.GameStart.Factories
{
    public static class ActionResolverFactory
    {
        private static IActionResolverBuilder _builder = new ActionResolverBuilder();
        //use maybe type
        public static IRequiredSource Create(ActionResolverType type, IEventManager eventManager)
        {
            switch (type)
            {
                case ActionResolverType.BurningFlames:
                    return _builder.ActionResolver(
                        new ProjectileSpellActionResolver(
                            new VFXService(), 
                            5, 
                            Element.Fire,
                            eventManager, 
                            ParticlePaths.FireBall, 
                            ParticlePaths.Explosion));

                case ActionResolverType.DarkBurningFlames:
                    return _builder.ActionResolver(
                        new ProjectileSpellActionResolver(
                            new VFXService(), 
                            5, 
                            Element.Fire, 
                            eventManager, 
                            ParticlePaths.DarkFireball, 
                            ParticlePaths.DarkExplosion));
                    
            }
            return null;
        }
    }

    public enum ActionResolverType
    {
        BurningFlames,
        DarkBurningFlames
    }
}
