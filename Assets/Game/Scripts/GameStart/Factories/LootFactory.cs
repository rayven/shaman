﻿using System.Collections.Generic;
using Scripts.Models.Actors;
using Scripts.Models.Cards;
using Scripts.Models.Events;
using Scripts.Models.Resolvers;
using Scripts.Models.Resolvers.CardsResolvers;
using Scripts.Models.Targetabilities;
using Scripts.Services.Loot;
using Scripts.Services.Spells;
using Scripts.Models.Elements;
using Scripts.Utils.Constants;
using System;
using Assets.Game.Scripts.Models.Resolvers.Builders;

namespace Scripts.GameStart.Factories
{
    public class LootFactory
    {
        [Inject]
        public IVFXService IvfxService { get; set; }

        private Ally _player;
        private IEventManager _eventManager;
        private IActionResolverBuilder _builder;

        public LootFactory(IEventManager eventManager, Ally player)
        {
            _player = player;
            _eventManager = eventManager;
            _builder = new ActionResolverBuilder();
        }

        public Dictionary<ILootable, float> CreateLootDictionary()
        {
            var loots = new Dictionary<ILootable, float>();

            for (int i = 1; i <= 3; i++)
            {
                var actionResolver = _builder
                    .ActionResolver(
                        new ProjectileSpellActionResolver(
                            new VFXService(),
                            20,
                            Element.Fire,
                            _eventManager,
                            ParticlePaths.FireBall,
                            ParticlePaths.Explosion))
                    .WithSource(_player);

                loots.Add(
                    new Card(
                       Guid.NewGuid(),
                     new List<IRequiredTargets>() { actionResolver },
                     "Burning Flames",
                     "GET REKT!",
                     "Path/Pour/Image/Feu",
                     2,
                     TargettingCriterions.SpellEffect,
                     TargettingCriterions.Foes,
                        (s) => true
                    ), 0.2f * i
                );
            }

            return loots;
        } 
    }
}
