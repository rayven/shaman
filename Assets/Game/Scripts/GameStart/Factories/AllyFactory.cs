﻿using System.Collections.Generic;
using Assets.Game.Scripts.Models.Resolvers.Builders;
using Scripts.Behaviours.Attack;
using Scripts.Behaviours.Look;
using Scripts.Behaviours.Movement;
using Scripts.Behaviours.Navigation;
using Scripts.Behaviours.Teleport;
using Scripts.Models.Actors;
using Scripts.Models.Resolvers;
using Scripts.Models.Targetabilities;
using Scripts.Utils.Constants;
using Scripts.Utils.Enums;
using UnityEngine;
using Scripts.Models.Elements;
using Scripts.Models.Events;

namespace Scripts.GameStart.Factories
{
    public static class AllyFactory
    {
        private static IActionResolverBuilder _builder = new ActionResolverBuilder();
        //return maybe type
        public static Ally Create(Enums.AllyType type, GameObject allyGameObject, IEventManager eventManager)
        {
            var moveBehaviour = allyGameObject.GetComponent<IMoveBehaviour>();
            var navigationBehaviour = allyGameObject.GetComponent<INavigationBehaviour>();
            var lookBehaviour = allyGameObject.GetComponent<ILookBehaviour>();
            var teleportBehaviour = allyGameObject.GetComponent<ITeleportBehaviour>();
            var attackBehaviour = allyGameObject.GetComponent<IAttackBehaviour>();

            switch (type)
            {
                    case Enums.AllyType.Torn:
                        return new Ally(
                            new ElementsSystem(Element.Wood, Element.Wood, Element.Wood, Element.Wood), 
                            new[] { _builder.ActionResolver(
                                new AttackActionResolver(
                                    Constants.AttackCooldown, 
                                    eventManager, 
                                    Element.Stone, 
                                    mustWaitCasting: false)) }, 
                            attackBehaviour,
                            moveBehaviour,
                            navigationBehaviour,
                            lookBehaviour,
                            teleportBehaviour,
                            TargettingCriterions.Sufferable,
                            TargettingCriterions.Sufferable,
                            speed: 6.0f,
                            jumpPower: 8.0f,
                            health: 30,
                            baseDamage: 100
                        );

                    case Enums.AllyType.Maiken:
                        return new Ally(
                            new ElementsSystem(Element.Stone, Element.Stone, Element.Stone, Element.Stone),
                            new[] { _builder.ActionResolver(
                                new AttackActionResolver(
                                    Constants.AttackCooldown,
                                    eventManager,
                                    Element.Stone,
                                    mustWaitCasting: false)) },
                            attackBehaviour,
                            moveBehaviour,
                            navigationBehaviour,
                            lookBehaviour,
                            teleportBehaviour,
                            TargettingCriterions.Sufferable,
                            TargettingCriterions.Sufferable,
                            speed: 6.0f,
                            jumpPower: 8.0f,
                            health: 20,
                            baseDamage: 100
                        );
            }

            return null;
        }
    }
}
