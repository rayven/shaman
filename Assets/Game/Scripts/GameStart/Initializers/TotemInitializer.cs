﻿using System.Collections.Generic;
using Assets.Game.Scripts.Models.Entities;
using UnityEngine;

namespace Assets.Game.Scripts.GameStart.Initializers
{
    public class TotemInitializer
    {
        public List<Totem> GetTotems(ushort totemCount)
        {
            var totems = new List<Totem>();
            switch (totemCount)
            {
                case 1: 
                    totems.Add(new Totem(new Vector3(-5, 0, 0), size:3));
                    break;
                case 2:
                    totems.Add(new Totem(new Vector3(-5, 0, -5), size:3));
                    totems.Add(new Totem(new Vector3(-5, 0, 5), size:3));
                    break;
            }
            
            return totems;
        }
    }
}
