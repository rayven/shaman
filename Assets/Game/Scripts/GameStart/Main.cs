﻿using System.Collections.Generic;
using Assets.Game.Scripts.Controllers;
using Assets.Game.Scripts.GameStart.Initializers;
using Scripts.Controllers;
using Scripts.GameStart.Factories;
using Scripts.Managers.Battle;
using Scripts.Managers.Camera;
using Scripts.Managers.Game;
using Scripts.Managers.Inputs;
using Scripts.Managers.Respawn;
using Scripts.Models;
using Scripts.Models.Actors;
using Scripts.Models.Cards;
using Scripts.Models.Entities;
using Scripts.Models.Events;
using Scripts.Models.Resolvers;
using Scripts.Services.Battles;
using Scripts.Services.Cursors;
using Scripts.Services.Loot;
using Scripts.Services.Pick;
using Scripts.Services.Selection;
using Scripts.Services.Spells;
using Scripts.Utils.Constants;
using Scripts.Utils.Enums;
using Scripts.Views;
using Scripts.Views.GameCanvas;
using UnityEngine;
using Scripts.Behaviours.Cameras;
using Scripts.Views.HealthBar;
using Scripts.Views.Status;
using Assets.Game.Scripts.Models.Cards;
using Assets.Game.Scripts.Models.Entities;
using Assets.Game.Scripts.Views.Entities;
using Scripts.Models.Blackboards;
using Scripts.Services.Totems;

namespace Scripts.GameStart
{
    public class Main : SignalContext
    {
        private LootFactory _lootFactory;
        private LootableFactory _lootableFactory;
        private EventManager _eventManager;
        private CameraManager _cameraManager;
        private GameManager _gameManager;
        private RespawnManager _respawnManager;
        private BattleController _battleController;
        private VFXService _vfxService;
        private RespawnCanvasView _respawnCanvasView;
        private LootCanvasView _lootCanvasView;
        private PickPhaseCanvasView _pickPhaseCanvasView;
        private TotemInitializer _totemInitializer;
        private TotemService _totemService;
        private BattleBlackboard _battleBlackboard;

        public Main(MonoBehaviour contextView) : base(contextView)
        { }

        protected override void mapBindings()
        {
            base.mapBindings();
            commandBinder.Bind<StartSignal>().To<StartCommand>().Once();

            _eventManager = new EventManager();
            _gameManager = new GameManager();
            _respawnManager = new RespawnManager();
            _battleController = new BattleController();
            _lootableFactory = new LootableFactory();
            _vfxService = new VFXService();
            _totemInitializer = new TotemInitializer();
            _battleBlackboard = new BattleBlackboard();
            _totemService = new TotemService();

            var tornObject = GameObject.Instantiate(Resources.Load(Paths.TornModel, typeof(GameObject))) as GameObject;
            var tornView = tornObject.GetComponent<TornView>();
            var tornOrthographicHealthBarView = tornObject.GetComponentInChildren<OrthographicHealthBarView>();
            var tornCooldownIndicator = tornObject.GetComponentInChildren<CooldownIndicatorView>();
            var torn = InitializeTorn(tornObject, _eventManager);
            tornOrthographicHealthBarView.Actor = torn;
            tornCooldownIndicator.Actor = torn;

            var maikenObject = GameObject.FindGameObjectWithTag("Player");
            var maikenView = maikenObject.GetComponent<MaikenView>();
            var maikenOrthographicHealthBarView = maikenObject.GetComponentInChildren<OrthographicHealthBarView>();
            var maikenCooldownIndicator = maikenObject.GetComponentInChildren<CooldownIndicatorView>();
            var maiken = InitializePlayer(maikenObject, _eventManager);
            maikenOrthographicHealthBarView.Actor = maiken;
            maikenCooldownIndicator.Actor = maiken;

            torn.Deck = CreateDeck(maiken, _eventManager);
            maiken.Deck = CreateDeck(maiken, _eventManager);

            _lootFactory = new LootFactory(_eventManager, maiken);

            var hand = new Hand();
            hand.EventManager = _eventManager;

            var bag = new Bag(_eventManager, 20);

            var cameraObject = GameObject.FindGameObjectWithTag("MainCamera");
            var sideXlsCameraBehaviour = cameraObject.GetComponent<SideExtremeLongShotCameraBehaviour>();
            var battleCameraBehaviour = cameraObject.GetComponent<BattleCameraBehaviour>();
            var cameraBehaviours = new Dictionary<CameraMode, ICameraBehaviour>();
            cameraBehaviours.Add(CameraMode.SideExtremeLongShot, sideXlsCameraBehaviour);
            cameraBehaviours.Add(CameraMode.BattleShot, battleCameraBehaviour);
            _cameraManager = InitializeCameraManager(maiken, cameraObject, cameraBehaviours);

            var respawnCanvasObject = GameObject.Instantiate(
                Resources.Load(Paths.RespawnCanvas, typeof(GameObject))) as GameObject;
            _respawnCanvasView = respawnCanvasObject.GetComponent<RespawnCanvasView>();
            var pickPhaseCanvasObject = GameObject.Instantiate(
                Resources.Load(Paths.PickPhaseCanvas, typeof(GameObject))) as GameObject;
            _pickPhaseCanvasView = pickPhaseCanvasObject.GetComponent<PickPhaseCanvasView>();
            var lootCanvasObject = GameObject.Instantiate(
                Resources.Load(Paths.LootCanvas, typeof (GameObject))) as GameObject;
            _lootCanvasView = lootCanvasObject.GetComponent<LootCanvasView>();
            var battleStartDecksObject = GameObject.Instantiate(
                Resources.Load(Paths.BattleStartDecks, typeof (GameObject))) as GameObject;
            var handCanvasObject = GameObject.Instantiate(
                Resources.Load(Paths.HandCanvas, typeof (GameObject))) as GameObject;

            _totemInitializer.GetTotems(2).ForEach(t =>
            {
                _totemService.Add(t);
                t.Reset();
            });
            
            injectionBinder.Bind<IEventManager>().ToValue(_eventManager).ToSingleton();
            injectionBinder.Bind<Ally>().ToValue(maiken).ToSingleton().ToName("Maiken");
            injectionBinder.Bind<Ally>().ToValue(torn).ToSingleton().ToName("Torn");
            injectionBinder.Bind<RespawnManager>().ToValue(_respawnManager).ToSingleton();
            injectionBinder.Bind<Hand>().ToValue(hand).ToSingleton();
            injectionBinder.Bind<LootCalculationService>().To<LootCalculationService>().ToSingleton();
            injectionBinder.Bind<TotemService>().To<TotemService>().ToSingleton();
            injectionBinder.Bind<BattleBlackboard>().ToValue(_battleBlackboard).ToSingleton();
            injectionBinder.Bind<ICameraManager>().ToValue(_cameraManager).ToSingleton();
            injectionBinder.Bind<IGameManager>().ToValue(_gameManager).ToSingleton();
            injectionBinder.Bind<IInputManager>().To<SideScrollerInputManager>().ToSingleton().ToName(GameState.SideScroller);
            injectionBinder.Bind<IInputManager>().To<BattleInputManager>().ToSingleton().ToName(GameState.Battle);
            injectionBinder.Bind<IController>().To<SideScrollerController>().ToSingleton().ToName(GameState.SideScroller);
            injectionBinder.Bind<IController>().To<BattleController>().ToSingleton().ToName(GameState.Battle);
            injectionBinder.Bind<ICameraBehaviour>().ToValue(sideXlsCameraBehaviour).ToName(CameraMode.SideExtremeLongShot);
            injectionBinder.Bind<ICameraBehaviour>().ToValue(battleCameraBehaviour).ToName(CameraMode.BattleShot);
            injectionBinder.Bind<ICursorService>().To<CursorService>().ToSingleton();

            var controllers = new Dictionary<GameState, IController>();
            var controller = injectionBinder.GetInstance<IController>(GameState.Battle);
            controllers.Add(GameState.Battle, injectionBinder.GetInstance<IController>(GameState.Battle));
            controllers.Add(GameState.PickPhase, new PickPhaseController());
            controllers.Add(GameState.SideScroller, injectionBinder.GetInstance<IController>(GameState.SideScroller));
            injectionBinder.Bind<Dictionary<GameState, IController>>().ToValue(controllers).ToSingleton();

            _battleController = injectionBinder.GetInstance<IController>(GameState.Battle) as BattleController;

            var handView = GameObject.FindGameObjectWithTag("Hand").GetComponent<HandView>();

            InitializeBattlegrounds(
                maiken,
                torn,
                bag,
                maikenView,
                tornView,
                handView,
                hand,
                maikenOrthographicHealthBarView,
                tornOrthographicHealthBarView);
        }

        private static CameraManager InitializeCameraManager(Ally maiken, GameObject cameraObject, Dictionary<CameraMode, ICameraBehaviour> cameraBehaviours)
        {
            foreach(var cameraBehaviour in cameraBehaviours)
            {
                cameraBehaviour.Value.Init(maiken);
            }

            return new CameraManager(
                cameraBehaviours,
                CameraMode.SideExtremeLongShot);
        }

        protected Deck CreateDeck(Ally Maiken, IEventManager eventManager)
        {
            var allCard = new Catalog.RawCatalog(Maiken, null,  eventManager, _vfxService);

            var cards = new List<Card>();
            foreach (KeyValuePair<System.Guid, Card> kv in allCard.AllCards)
            {
                cards.Add(kv.Value);
            }
            foreach (KeyValuePair<System.Guid, Card> kv in allCard.AllCards)
            {
                cards.Add(kv.Value);
            }
            foreach (KeyValuePair<System.Guid, Card> kv in allCard.AllCards)
            {
                cards.Add(kv.Value);
            }

            var deck = new Deck(cards);
            return deck;
        }

        protected Ally InitializeTorn(GameObject tornObject, EventManager eventManager)
        {
            tornObject.transform.position = new Vector3(-10, -10, -10);
            
            return AllyFactory.Create(Enums.AllyType.Torn, tornObject, _eventManager);
        }

        protected Ally InitializePlayer(GameObject maikenObject, EventManager eventManager)
        {
            return AllyFactory.Create(Enums.AllyType.Maiken, maikenObject, _eventManager);
        }

        protected void InitializeEnemy(
            IEventAggregator eventAggregator,
            Vector3 position,
            GameObject enemyGameObject,
            Ally maiken,
            Ally torn,
            Battleground battleground)
        {
            var enemyView = enemyGameObject.GetComponent<EnemyView>();
            var healthBarView = enemyGameObject.GetComponentInChildren<OrthographicHealthBarView>();
            var cooldownIndicatorView = enemyGameObject.GetComponentInChildren<CooldownIndicatorView>();
            eventAggregator.Subscribe(healthBarView);
            var enemy = EnemyFactory.CreateEnemy(
                enemyView.Type,
                enemyGameObject,
                eventAggregator, 
                _eventManager,
                maiken,
                torn);

            enemy.BattleBlackboard = _battleBlackboard;
            enemy.Maiken = maiken;
            enemy.Torn = torn;
            enemyView.EventAggregator = eventAggregator;
            enemyView.Enemy = enemy;
            healthBarView.Actor = enemy;
            cooldownIndicatorView.Actor = enemy;
        }

        protected void InitializeBattlegrounds(
            Ally maiken,
            Ally torn,
            Bag bag,
            MaikenView maikenView,
            TornView tornView,
            HandView handView,
            Hand hand,
            OrthographicHealthBarView maikenOrthographicHealthBarView,
            OrthographicHealthBarView tornOrthographicHealthBarView)
        {
            var battlegroundObjects = GameObject.FindGameObjectsWithTag("Battleground");

            foreach (var battlegroundObject in battlegroundObjects)
            {
                var eventAggregator = SetupNewEventAggregator(maiken, torn, bag, maikenView, tornView, handView, hand, maikenOrthographicHealthBarView, tornOrthographicHealthBarView);

                InitializeBattleground(eventAggregator, battlegroundObject, maiken, torn);
            }
        }

        private void InitializeBattleground(
            EventAggregator eventAggregator,
            GameObject battlegroundObject,
            Ally maiken, 
            Ally torn)
        {
            var battlegroundView = battlegroundObject.GetComponent<BattlegroundView>();
            eventAggregator.Subscribe(battlegroundView);

            battlegroundView.Player = maiken;
            battlegroundView.LootableFactory = _lootableFactory;
            var battleground = new Battleground(
                maiken, 
                torn, 
                eventAggregator, 
                _lootFactory.CreateLootDictionary(),
                new DownAllEnemiesVictoryActionResolver(_eventManager));
            var position = battlegroundObject.transform.position;

            battleground.Position = position;
            battlegroundView.Battleground = battleground;

            for (var i = 0; i < battlegroundObject.transform.childCount; i++)
            {
                var enemyObject = battlegroundObject.transform.GetChild(i).gameObject;
                if (enemyObject.tag == "Enemy")
                {
                    InitializeEnemy(eventAggregator, position, enemyObject, maiken, torn, battleground);
                }
            }
        }

        private EventAggregator SetupNewEventAggregator(
            Ally maiken, 
            Ally torn, 
            Bag bag,
            MaikenView maikenView, 
            TornView tornView, 
            HandView handView, 
            Hand hand, 
            OrthographicHealthBarView maikenOrthographicHealthBarView,
            OrthographicHealthBarView tornOrthographicHealthBarView)
        {
            var eventAggregator = new EventAggregator();

            eventAggregator.Subscribe(_gameManager);
            eventAggregator.Subscribe(_cameraManager);
            eventAggregator.Subscribe(_respawnManager);

            eventAggregator.Subscribe(maiken);
            eventAggregator.Subscribe(torn);
            eventAggregator.Subscribe(hand);
            eventAggregator.Subscribe(bag);

            eventAggregator.Subscribe(_battleController);

            eventAggregator.Subscribe(handView);
            eventAggregator.Subscribe(tornView);
            eventAggregator.Subscribe(maikenView);
            eventAggregator.Subscribe(_respawnCanvasView);
            eventAggregator.Subscribe(_pickPhaseCanvasView);
            eventAggregator.Subscribe(_lootCanvasView);
            eventAggregator.Subscribe(maikenOrthographicHealthBarView);
            eventAggregator.Subscribe(tornOrthographicHealthBarView);

            eventAggregator.Subscribe(_vfxService);
            eventAggregator.Subscribe(_totemService);
            eventAggregator.Subscribe(_battleBlackboard);
            var battleService = new BattleService(eventAggregator);
            var selectionDispatcher = new SelectionDispatcher(eventAggregator);
            var cardPickDispatcher = new CardPickDispatcher(eventAggregator);
            var battleManager = new BattleManager(eventAggregator, battleService);

            return eventAggregator;
        }
    }
}