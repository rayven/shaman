﻿using System.Collections.Generic;
using Assets.Game.Scripts.Models.Resolvers.Builders;
using Scripts.Models.Elements;
using Scripts.Behaviours.Attack;
using Scripts.Behaviours.Chase;
using Scripts.Behaviours.Look;
using Scripts.Behaviours.Navigation;
using Scripts.Behaviours.Patrol;
using Scripts.Behaviours.Teleport;
using Scripts.GameStart.Factories;
using Scripts.Models.Actors;
using Scripts.Models.Events;
using Scripts.Models.Resolvers;
using Scripts.Models.Targetabilities;
using Scripts.Views;
using UnityEngine;
using Scripts.Models.AI;
using Scripts.Models.Blackboards;

namespace Scripts.GameStart
{
    public static class EnemyFactory
    {
        public static Enemy CreateEnemy(
            EnemyType type, 
            GameObject enemyGameObject,
            IEventAggregator eventAggregator, 
            IEventManager eventManager,
            Ally maiken,
            Ally torn)
        {
            var chaseBehaviour = enemyGameObject.GetComponent<IChaseBehaviour>();
            var patrolBehaviour = enemyGameObject.GetComponent<IPatrolBehaviour>();
            var navigationBehaviour = enemyGameObject.GetComponent<INavigationBehaviour>();
            var lookBehaviour = enemyGameObject.GetComponent<ILookBehaviour>();
            var teleportBehaviour = enemyGameObject.GetComponent<ITeleportBehaviour>();
            var attackBehaviour = enemyGameObject.GetComponent<IAttackBehaviour>();

            switch (type)
            {
                case EnemyType.Uraki:
                    var actionResolver = ActionResolverFactory.Create(ActionResolverType.DarkBurningFlames, eventManager);

                    var enemy = new Enemy(
                        new ElementsSystem(Element.Wood, Element.Wood, Element.Wood, Element.Wood), 
                        eventAggregator,
                        attackBehaviour,
                        chaseBehaviour,
                        patrolBehaviour,
                        navigationBehaviour,
                        lookBehaviour,
                        teleportBehaviour,
                        TargettingCriterions.Sufferable,
                        TargettingCriterions.Sufferable,
                        speed: 3.1f,
                        minSightDistance: 2,
                        health: 50,
                        baseDamage: 10);

                    enemy.AddAIComponent(new CooldownAIComponent(eventManager, 5.0f), actionResolver);
                    return enemy;
                default:

                    return new Enemy(
                        new ElementsSystem(Element.Stone, Element.Stone, Element.Stone, Element.Stone), 
                        eventAggregator,
                        attackBehaviour,
                        chaseBehaviour,
                        patrolBehaviour,
                        navigationBehaviour,
                        lookBehaviour,
                        teleportBehaviour,
                        TargettingCriterions.Sufferable,
                        TargettingCriterions.Sufferable,
                        speed: 8.0f,
                        minSightDistance: 2,
                        health: 50,
                        baseDamage: 10);
            }
        }
    }
}
