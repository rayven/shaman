﻿using strange.extensions.context.impl;

namespace Scripts.GameStart
{
    public class Bootstrap : ContextView
    {
        public void Awake()
        {
            this.context = new Main(this);
        }
    }
}
