﻿using Scripts.Behaviours.Cameras;
using Scripts.Models.Events;
using System.Collections.Generic;

namespace Scripts.Managers.Camera
{
    public class CameraManager : ICameraManager, ISubscriber<BattleStarted>, ISubscriber<BattleEnded>
    {
        private Dictionary<CameraMode, ICameraBehaviour> _cameraBehaviours;
        private ICameraBehaviour _currentCameraBehaviour;

        public CameraManager(
            Dictionary<CameraMode, ICameraBehaviour> cameraBehaviours,
            CameraMode currentCameraMode)
        {
            _cameraBehaviours = cameraBehaviours;
            ChangeCameraMode(currentCameraMode);
        }

        public void UpdateCurrentState()
        {
            _currentCameraBehaviour.Follow();
        }

        public void ChangeCameraMode(CameraMode mode)
        {
            _currentCameraBehaviour = _cameraBehaviours[mode];
        }
        
        public void OnEvent(BattleStarted e)
        {
            ChangeCameraMode(CameraMode.BattleShot);
        }

        public void OnEvent(BattleEnded e)
        {
            ChangeCameraMode(CameraMode.SideExtremeLongShot);
        }
    }
}
