﻿using Scripts.Behaviours.Cameras;

namespace Scripts.Managers.Camera
{
    public interface ICameraManager
    {
        void ChangeCameraMode(CameraMode mode);
        void UpdateCurrentState();
    }
}
