﻿using Scripts.Models.Actors;
using Scripts.Models.Events;
using UnityEngine;

namespace Scripts.Managers.Respawn
{
    public class RespawnManager : ISubscriber<RespawnRequested>
    {
        [Inject("Maiken")]
        public Ally Maiken { get; set; }
        public Vector3 LastRespawnPosition { get; set; }

        public void OnEvent(RespawnRequested e)
        {
            Maiken.TeleportBehaviour.Teleport(LastRespawnPosition);
        }
    }
}
