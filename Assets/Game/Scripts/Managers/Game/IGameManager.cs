﻿using Scripts.Controllers;

namespace Scripts.Managers.Game
{
    public interface IGameManager
    {
        IController CurrentController { get; set; }
        
        void Update();
        void FixedUpdate();
        void ChangeState(GameState state);
    }

    public enum GameState
    {
        SideScroller,
        Battle,
        PickPhase,
    }
}
