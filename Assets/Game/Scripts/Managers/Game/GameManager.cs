﻿using System.Collections.Generic;
using Assets.Game.Scripts.Controllers;
using Scripts.Controllers;
using Scripts.Models.Events;

namespace Scripts.Managers.Game
{
    public class GameManager 
        : IGameManager
        , ISubscriber<BattleStarted>
        , ISubscriber<PickPhaseFinished>
        , ISubscriber<BattleEnded>
    {
        [Inject]
        public Dictionary<GameState, IController> Controllers { get; set; }

        [Inject(GameState.SideScroller)]
        public IController CurrentController { get; set; }

        public void Update()
        {
            CurrentController.Update();
        }

        public void FixedUpdate()
        {
            CurrentController.FixedUpdate();
        }
    
        public void ChangeState(GameState state)
        {
            CurrentController = Controllers[state];
        }

        public void OnEvent(BattleStarted e)
        {
            ChangeState(GameState.PickPhase);
        }

        public void OnEvent(BattleEnded e)
        {
            ChangeState(GameState.SideScroller);
        }

        public void OnEvent(PickPhaseFinished e)
        {
            ChangeState(GameState.Battle);
        }
    }
}
