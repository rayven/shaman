﻿using Scripts.Models.Events;
using Scripts.Services.Battles;
using Scripts.Services.Loot;

namespace Scripts.Managers.Battle
{
    public class BattleManager : IBattleManager, 
        ISubscriber<BattleStartRequested>, 
        ISubscriber<LootCalculationRequested>, 
        ISubscriber<PlayerLost>
    {
        private readonly IEventAggregator _eventAggregator;
        private readonly BattleService _battleService;
        private readonly LootCalculationService _lootService;

        public BattleManager(IEventAggregator eventAggregator, BattleService battleService)
        {
            _eventAggregator = eventAggregator;
            _eventAggregator.Subscribe(this);
            _battleService = battleService;
            _lootService = new LootCalculationService();
        }

        public void OnEvent(BattleStartRequested e)
        {
            _eventAggregator.Publish(new BattleServiceChanged(_battleService));
        }

        public void OnEvent(LootCalculationRequested e)
        {
            var loot = _lootService.GetLoot(e.LootablesWithPercentageChances);
            _eventAggregator.Publish(new ShowLootRequested(loot));
        }

        public void OnEvent(PlayerLost e)
        {
            _eventAggregator.Publish(new RespawnCanvasRequested());
        }
    }
}
