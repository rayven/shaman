﻿using System.Collections.Generic;
using Scripts.Commands.SideScroller;
using Scripts.Models.Actors;
using Scripts.Services.Commands;
using UnityEngine;

namespace Scripts.Managers.Inputs
{
    public class SideScrollerInputManager : IInputManager
    {
        [Inject("Maiken")]
        public Ally Player { get; set; }

        public List<ICommand> Handle()
        {            
            var commands = new List<ICommand>();
            var direction = Vector3.zero;

            if (Input.GetKey(KeyCode.W))
            {
                direction += Vector3.forward;
            }

            if (Input.GetKey(KeyCode.S))
            {
                direction += Vector3.back;
            }

            if (Input.GetKey(KeyCode.D))
            {
                direction += Vector3.right;
            }

            if (Input.GetKey(KeyCode.A))
            {
                direction += Vector3.left;
            }

            //if (Input.GetKeyDown(KeyCode.Space)) {
            //    commands.Add(new JumpPlayerCommand(Player));
            //}

            if (direction != Vector3.zero)
            {
                direction.Normalize();
                commands.Add(new MovePlayerCommand(Player, direction));
            }

            return commands;
        }
    }
}
