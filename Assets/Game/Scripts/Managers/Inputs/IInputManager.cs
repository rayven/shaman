﻿using System.Collections.Generic;
using Scripts.Services.Commands;

namespace Scripts.Managers.Inputs
{
    public interface IInputManager
    {
        List<ICommand> Handle();
    }
}
