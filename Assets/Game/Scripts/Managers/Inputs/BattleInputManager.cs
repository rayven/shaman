﻿using System.Collections.Generic;
using Assets.Game.Scripts.Commands.Battle;
using Scripts.Models.Cards;
using Scripts.Models.Events;
using Scripts.Services;
using Scripts.Services.Commands;
using Scripts.Services.Cursors;
using UnityEngine;

namespace Scripts.Managers.Inputs
{
    public class BattleInputManager : IInputManager
    {
        private const int KEY_ONE_CODE = (int) KeyCode.Alpha1;

        [Inject]
        public ICursorService CursorService { get; set; }

        [Inject]
        public Hand Hand { get; set; }

        [Inject]
        public IEventManager EventManager { get; set; }

        public List<ICommand> Handle()
        {
            var e = Event.current;
            var commands = new List<ICommand>();

            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                commands.Add(PlayCard((int) KeyCode.Alpha1 - KEY_ONE_CODE));
            }

            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                commands.Add(PlayCard(GetIndexFromHand(KeyCode.Alpha2)));
            }

            if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                commands.Add(PlayCard(GetIndexFromHand(KeyCode.Alpha3)));
            }

            if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                commands.Add(PlayCard(GetIndexFromHand(KeyCode.Alpha4)));
            }

            if (Input.GetKeyDown(KeyCode.Alpha5))
            {
                commands.Add(PlayCard(GetIndexFromHand(KeyCode.Alpha5)));
            }

            if (Input.GetKeyDown(KeyCode.Alpha6))
            {
                commands.Add(PlayCard(GetIndexFromHand(KeyCode.Alpha6)));
            }

            if (e.button == 0 && e.type == EventType.MouseDown)
            {
                commands.Add(new SelectActorCommand(CursorService));
            }

            if (Input.GetKeyDown(KeyCode.Mouse1))
            {
                commands.Add(new PickCardCommand(CursorService));
            }

            return commands;
        }

        private int GetIndexFromHand(KeyCode keyCode)
        {
            return (int)keyCode - (int)KeyCode.Alpha1;
        }

        private PlayCardCommand PlayCard(int index)
        {
            return new PlayCardCommand(EventManager, Hand, index);
        }
}
}
