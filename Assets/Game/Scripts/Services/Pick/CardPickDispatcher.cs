﻿using Scripts.Models.Actors;
using Scripts.Models.Events;

namespace Scripts.Services.Pick
{
    public class CardPickDispatcher : ISubscriber<AllyPickCardRequested>
    {
        private readonly IEventAggregator _eventAggregator;

        public CardPickDispatcher(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            _eventAggregator.Subscribe(this);
        }

        public void OnEvent(AllyPickCardRequested e)
        {
            var actor = (Ally) e.Selectable;
            if (actor != null && actor.IsNotOnCooldown && actor.IsAlive)
            {
                _eventAggregator.Publish(new PickCardRequested(actor));
            }
        }
    }
}
