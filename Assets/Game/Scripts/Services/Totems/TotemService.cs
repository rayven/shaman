﻿using System.Collections.Generic;
using Assets.Game.Scripts.Models.Entities;
using UnityEngine;
using System;
using System.Linq;
using Assets.Game.Scripts.Views.Entities;
using Scripts.Models;
using Scripts.Models.Events;
using Scripts.Views.Particles;

namespace Scripts.Services.Totems
{
    public class TotemService 
        : ISubscriber<BattleEnded>
        , ISubscriber<SpawnTotemBasesRequested>

    {
        [Inject]
        public List<TotemView> TotemViews { get; set; }
        [Inject]
        public IEventManager EventManager { get; set; }

        public TotemService()
        {
            TotemViews = new List<TotemView>();
        }

        public void Initialize(Battleground battleground)
        {
            TotemViews.ForEach(go =>
            {
                var gameObject = go.gameObject;
                gameObject.SetActive(true);
                gameObject.transform.position = go.Totem.Position + battleground.Position;
            });
        }

        public void Reset()
        {
            TotemViews.ForEach(view =>
            {
                view.Totem.Reset();
                view.gameObject.SetActive(false);
            });
        }
        
        public void Add(Totem totem)
        {
            var gameObject = GameObject.Instantiate(Resources.Load("Totems/Base", typeof (GameObject))) as GameObject;
            gameObject.transform.position = totem.Position;
            var view = gameObject.GetComponent<TotemView>();
            view.Attach(totem);

            TotemViews.Add(view);
        }
        
        public void OnEvent(BattleEnded e)
        {
            Reset();
        }

        public void OnEvent(SpawnTotemBasesRequested e)
        {
            Initialize(e.Battleground);
        }
    }
}
