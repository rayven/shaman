﻿using Scripts.Views.Loot;
using UnityEngine;

namespace Scripts.Services.Loot
{
    public class LootableFactory
    {
        public GameObject Create(ILootable lootable, Vector3 position)
        {
            var information = lootable.GetLootInformation();
            var path = string.Format("Lootables/{0}", information.Type);
            var lootObject = GameObject.Instantiate(
                Resources.Load(
                    path
                  , typeof(GameObject))
                  , position
                  , Quaternion.identity) as GameObject;
            
            var lootableView = lootObject.AddComponent<LootableView>();
            lootableView.Lootable = lootable;
            
            return lootObject;
        }
    }
}
