﻿using System.Collections.Generic;
using System.Linq;

namespace Scripts.Services.Loot
{
    public class LootCalculationService
    {
        public List<ILootable> GetLoot(Dictionary<ILootable, float> lootablesWithPercentageChance)
        {
            //RUN ALGO THAT SELECT 3 ILootable in dictionary
            return lootablesWithPercentageChance.Select(x => x.Key).ToList();
        }
    }

    public interface ILootable
    {
        void Dispose();
        LootInformation GetLootInformation();
    }

    public class LootInformation
    {
        public string Title { get; set; }
        public string Type { get; set; }
    }
}
