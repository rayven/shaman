﻿using System;
using Scripts.Views;
using Scripts.Views.Particles;
using UnityEngine;

namespace Scripts.Services.Spells
{
    public interface IVFXService
    {
        VFXService Instantiate(string type, Vector3 position);
        VFXService WithCallback(Action callback);
        VFXService AsProjectileWithCallback(Vector3 destination, Action callback);
    }

    public class VFXService : IVFXService
    {
        private GameObject _spell;

        public VFXService Instantiate(string type, Vector3 position)
        {
            _spell = GameObject.Instantiate(
                Resources.Load(
                    type
                  , typeof(GameObject))
                  , position
                  , Quaternion.identity) as GameObject;

            return this;
        }

        public VFXService WithCallback(Action callback)
        {
            var callbackView = _spell.AddComponent<ParticleCallbackBehaviour>();

            callbackView.Callback = callback;
            
            return this;
        }

        public VFXService AsProjectileWithCallback(Vector3 destination, Action callback)
        {
            var projectileSpellView = _spell.AddComponent<ProjectileSpellBehaviour>();

            projectileSpellView.Destination = destination;
            projectileSpellView.Callback = callback;

            return this;
        }
    }
    
    //VisualEffect_Caster_Target
    public enum FxType
    {
        PhysicalAttack,
        Flames_Single_Single,
        Leaves_Single_Single,
        Cooldown_Reset
    }
}
