﻿namespace Scripts.Services.Cursors
{
    public interface ICursorService
    {
        T GetMouseOver<T>();
    }
}
