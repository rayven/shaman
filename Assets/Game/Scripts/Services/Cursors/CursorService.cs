﻿using Scripts.Views;
using UnityEngine;

namespace Scripts.Services.Cursors
{
    public class CursorService : ICursorService
    {
        private RaycastHit _hit;

        //Use option<T> from C# language extension
        public T GetMouseOver<T>()
        {
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            Physics.Raycast(ray, out _hit);

            if (_hit.transform != null)
            {
                var clickable = _hit.transform.gameObject.GetComponent<T>();
                
                return clickable;
            }

            return default(T);
        }
    }
}
