﻿using System.Linq;
using Assets.dotNetExtensions;
using Scripts.Models.Events;
using Scripts.Models.viewModels;
using Scripts.Models.Targetabilities;
using Scripts.Models;
using Scripts.Models.Actors;
using Scripts.Models.Cards;

namespace Scripts.Services.Selection
{
    public class SelectionDispatcher : ISelectionDispatcher, 
        ISubscriber<SelectRequested>, 
        ISubscriber<PickPhaseFinished>, 
        ISubscriber<BattleStarted>,
        ISubscriber<ResetSelectionRequested>
    {
        private ISelectable _selectedObject;

        private readonly IEventAggregator _eventAggregator;
        private bool _isPickPhaseFinished;

        public SelectionDispatcher(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            _eventAggregator.Subscribe(this);
        }

        private void SelectSource(SelectRequested e)
        {
            if (!_isPickPhaseFinished) return;
            if (SelectableIsEnemy(e)) return;
            if (SelectableIsSufferable(e))
            {
                if (SelectableIsOnCooldown(e)) return;
                if (SelectableIsDead(e)) return;
            }

            _eventAggregator.Publish(new ResetTargetablesRequested());
            _selectedObject = e.RequestedSelectable;
            _selectedObject.Select();
            _eventAggregator.Publish(new TargetRequested(_selectedObject.TargetCriterions, _selectedObject.Allegiance));
        }


        public void SelectTarget(SelectRequested selectRequest)
        {
            if (SelectableIsSufferable(selectRequest) && SelectableIsDead(selectRequest)) { return; }
            var targetting = new TargetRequested(_selectedObject.TargetCriterions, _selectedObject.Allegiance);

            if (selectRequest.RequestedSelectable.ValidAllegiance(targetting))
            {
                var actionResolvers = _selectedObject
                    .GetActionResolvers()
                    .Select(ar => ar
                        .WithTargets(new[] { selectRequest.RequestedSelectable})
                        .Build())
                    .ToList();

                if (_selectedObject is Card)
                {
                    _eventAggregator.Publish(new CardPlayed(_selectedObject as Card));
                }
                _eventAggregator.Publish(new ActionResolverPushed(actionResolvers));

                ResetSelection();
                return;
            }

            if (_selectedObject != selectRequest.RequestedSelectable)
            {
                ResetSelection();
                SelectSource(selectRequest);
            }
            else
            {
                ResetSelection();
            }
        }
        
        public void OnEvent(SelectRequested e)
        {
            if (_selectedObject == null)
            {
                SelectSource(e);
            }
            else
            {
                SelectTarget(e);
            }
        }

        private bool SelectableIsEnemy(SelectRequested e)
        {
            return (e.RequestedSelectable.SelfCriterions & TargettingCriterions.Foes) == TargettingCriterions.Foes;
        }

        private bool SelectableIsSufferable(SelectRequested e)
        {
            return (e.RequestedSelectable.SelfCriterions & TargettingCriterions.Sufferable) == TargettingCriterions.Sufferable;
        }

        private bool SelectableIsOnCooldown(SelectRequested e)
        {
            var actor = (Actor)e.RequestedSelectable as Actor;
            return actor.IsOnCooldown;
        }

        private bool SelectableIsDead(SelectRequested e)
        {
            var actor = e.RequestedSelectable as Actor;
            return actor.IsDead;
        }

        public void ResetSelection()
        {
            _eventAggregator.Publish(new ResetTargetablesRequested());
            if (_selectedObject != null)
            {
                _selectedObject.UnSelect();
                _selectedObject = null;
            }
        }

        public void OnEvent(PickPhaseFinished e)
        {
            _isPickPhaseFinished = true;
        }

        public void OnEvent(BattleStarted e)
        {
            _isPickPhaseFinished = false;
        }

        public void OnEvent(ResetSelectionRequested e)
        {
            ResetSelection();
        }
    }
}
 