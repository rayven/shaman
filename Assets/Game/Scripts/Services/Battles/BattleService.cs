﻿using System.Collections.Generic;
using Scripts.Models.Events;
using Scripts.Models.Resolvers;

namespace Scripts.Services.Battles
{
    public class BattleService : IBattleService
        , ISubscriber<ActionResolverPushed>
        , ISubscriber<BattleStartRequested>
        , ISubscriber<RespawnCanvasRequested>
        , ISubscriber<ShowLootRequested>

    {
        private readonly IEventAggregator _eventAggregator;
        private readonly List<IActionResolver> _actionResolvers;
        private readonly List<IActionResolver> _resolversToRemove;

        public BattleService(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            _eventAggregator.Subscribe(this);

            _actionResolvers = new List<IActionResolver>();
            _resolversToRemove = new List<IActionResolver>();
        }
        
        public void Update()
        {
            _actionResolvers.ForEach(Handle);
            RemoveFinishedActionResolvers();
        }

        private void RemoveFinishedActionResolvers()
        {
            _resolversToRemove.ForEach(a => _actionResolvers.Remove(a));
            _resolversToRemove.Clear();
        }

        private void Handle(IActionResolver resolver)
        {
            if (resolver.IsFinished)
            {
                _resolversToRemove.Add(resolver);
                resolver.IsFinished = false;
            }
            else
            {
                resolver.Resolve();
            }
        }
        
        private void Reset()
        {
            _actionResolvers.Clear();
            _resolversToRemove.Clear();
        }

    #region Events

        public void OnEvent(ActionResolverPushed e)
        {
            e.ActionResolvers.ForEach(ar => _actionResolvers.Add(ar));
        }
        
        public void OnEvent(BattleStartRequested e)
        {
            Reset();
            _eventAggregator.Publish(new BattleStarted());
        }
        
        public void OnEvent(ShowLootRequested e)
        {
            Reset();
        }

        public void OnEvent(RespawnCanvasRequested e)
        {
            Reset();
        }

        #endregion
    }
}
