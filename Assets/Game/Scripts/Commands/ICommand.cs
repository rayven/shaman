﻿namespace Scripts.Services.Commands
{
    public interface ICommand
    {
        void Execute();
    }
}
