﻿using Scripts.Services.Commands;
using Scripts.Services.Cursors;
using Scripts.Views;
using UnityEngine;

namespace Assets.Game.Scripts.Commands.Battle
{
    public class PickCardCommand : ICommand
    {
        private ICursorService _cursorService;

        public PickCardCommand(ICursorService cursorService)
        {
            _cursorService = cursorService;
        }

        public void Execute()
        {
            var cardPickable = _cursorService.GetMouseOver<ICardPickable>();
            if (cardPickable != null)
            {
                cardPickable.PickCard();
            }
        }
    }
}
