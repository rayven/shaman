﻿using Scripts.Models.Cards;
using Scripts.Models.Events;
using Scripts.Services.Commands;

namespace Assets.Game.Scripts.Commands.Battle
{
    public class PlayCardCommand : ICommand
    {
        private readonly IEventManager _eventManager;
        private readonly Hand _hand;
        private readonly int _index;

        public PlayCardCommand(IEventManager eventManager, Hand hand, int index)
        {
            _eventManager = eventManager;
            _hand = hand;
            _index = index;
        }

        public void Execute()
        {
            _eventManager.CurrentEventAggregator.Publish(new SelectRequested(_hand.GetCard(_index)));
        }
    }
}
