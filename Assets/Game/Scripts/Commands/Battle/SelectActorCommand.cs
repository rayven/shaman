﻿using Scripts.Services.Commands;
using Scripts.Services.Cursors;
using Scripts.Views;

namespace Assets.Game.Scripts.Commands.Battle
{
    public class SelectActorCommand : ICommand
    {
        private ICursorService _cursorService;

        public SelectActorCommand(ICursorService cursorService)
        {
            _cursorService = cursorService;
        }

        public void Execute()
        {
            var clickable = _cursorService.GetMouseOver<IClickable>();
            if (clickable != null)
            {
                clickable.OnClick();
            }
        }
    }
}
