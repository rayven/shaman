﻿using Scripts.Models.Actors;
using Scripts.Services.Commands;

namespace Scripts.Commands.SideScroller
{
    public class JumpPlayerCommand : ICommand
    {
        private readonly Ally _player;

        public JumpPlayerCommand(Ally player)
        {
            _player = player;
        }

        public void Execute()
        {
            _player.Jump();
        }
   } 
}
