﻿using Scripts.Models.Actors;
using Scripts.Models.Entities;
using Scripts.Services.Commands;
using UnityEngine;

namespace Scripts.Commands.SideScroller
{
    public class MovePlayerCommand : ICommand
    {
        private readonly Vector3 _direction;
        private readonly Ally _player;

        public MovePlayerCommand(Ally player, Vector3 direction)
        {
            _direction = direction;
            _player = player;
        }

        public void Execute()
        {
            _player.Move(_direction);
        }
    }
}
