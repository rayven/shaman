﻿using Scripts.Managers.Game;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Scripts.Views
{
    public class ControllerView : View
    {
        [Inject]
        public IGameManager GameManager { get; set; }

        public void Update()
        {
            GameManager.Update();
        }
        
        public void OnGUI()
        {
            GameManager.FixedUpdate();
        }
    }
}
