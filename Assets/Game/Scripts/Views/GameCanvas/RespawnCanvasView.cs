﻿using Scripts.Models.Events;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Scripts.Views.GameCanvas
{
    public class RespawnCanvasView : View, ISubscriber<RespawnCanvasRequested>
    {
        [Inject]
        public IEventManager EventManager { get; set; }

        protected override void Awake()
        {
            gameObject.SetActive(false);
        }

        public void OnEvent(RespawnCanvasRequested e)
        {
            gameObject.SetActive(true);
        }

        public void OnClickRespawn()
        {
            EventManager.CurrentEventAggregator.Publish(new BattleEnded());
            EventManager.CurrentEventAggregator.Publish(new RespawnRequested());
            gameObject.SetActive(false);
        }
    }
}
