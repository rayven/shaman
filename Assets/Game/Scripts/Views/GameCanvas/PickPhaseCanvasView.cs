﻿using Scripts.Models.Actors;
using Scripts.Models.Events;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Scripts.Views.GameCanvas
{
    public class PickPhaseCanvasView 
        : View
        , ISubscriber<HandIsFull>
        , ISubscriber<BattleStarted>
    {
        [Inject]
        public IEventManager EventManager { get; set; }
        [Inject("Torn")]
        public Ally Torn { get; set; }
        [Inject("Maiken")]
        public Ally Maiken { get; set; }

        protected override void Awake()
        {
            gameObject.SetActive(false);
        }

        public void OnGUI()
        {
            if (GUI.Button(new Rect(Screen.width / 2 - 101, Screen.height / 2 - 15, 100, 30), "Maiken Deck"))
            {
                EventManager.CurrentEventAggregator.Publish(new PickCardRequested(Maiken));
            }
            if (GUI.Button(new Rect(Screen.width / 2 + 1, Screen.height / 2 - 15, 100, 30), "Torn Deck"))
            {
                EventManager.CurrentEventAggregator.Publish(new PickCardRequested(Torn));
            }
        }

        public void OnEvent(HandIsFull e)
        {
            gameObject.SetActive(false);
            EventManager.CurrentEventAggregator.Publish(new PickPhaseFinished());
        }

        public void OnEvent(BattleStarted e)
        {
            gameObject.SetActive(true);
        }
    }
}
