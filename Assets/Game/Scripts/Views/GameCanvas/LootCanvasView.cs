﻿using Scripts.Models.Events;
using Scripts.Services.Loot;
using Scripts.Utils.Constants;
using strange.extensions.mediation.impl;
using UnityEngine;
using UnityEngine.UI;

namespace Scripts.Views.GameCanvas
{
    public class LootCanvasView : View, 
        ISubscriber<LootCanvasRequested>, 
        ISubscriber<BattleEnded>
    {
        [Inject]
        public IEventManager EventManager { get; set; }

        public Transform LootGroup;

        protected override void Awake()
        {
            gameObject.SetActive(false);
        }

        public void OnGUI()
        {
            GUI.Label(new Rect(Screen.width/2, 10, 100, 30), "YOU WIN!");
        }

        public void OnEvent(LootCanvasRequested e)
        {
            gameObject.SetActive(true);
            
            foreach (var lootable in e.Lootables)
            {
                var lootButton = CreateLootButton();
                SetButtonImage(lootable.GetLootInformation().Title, lootButton);
                SetButtonClickEvent(lootable, lootButton);
            }
        }
        
        private void SetButtonClickEvent(ILootable lootable, GameObject lootButton)
        {
            var onClick = lootButton.GetComponent<LootOnClick>();
            onClick.Lootable = lootable;
        }

        private void SetButtonImage(string title, GameObject lootButton)
        {
            var image = lootButton.GetComponent<Image>();
            image.sprite = LoadSprite(FormatToCardImagePath(title));
        }

        private GameObject CreateLootButton()
        {
            var lootButton = GameObject.Instantiate(Resources.Load(Paths.LootButton, typeof(GameObject))) as GameObject;
            lootButton.transform.SetParent(LootGroup);

            return lootButton;
        }

        private string FormatToCardImagePath(string title)
        {
            return string.Format("Cards/Images/{0}", title);
        }

        private Sprite LoadSprite(string path)
        {
            return (Sprite) Resources.Load(path, typeof(Sprite));
        }

        public void OnEvent(BattleEnded e)
        {
            gameObject.SetActive(false);
        }
    }
}
