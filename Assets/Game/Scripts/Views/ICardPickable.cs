﻿namespace Scripts.Views
{
    public interface ICardPickable
    {
        void PickCard();
    }
}
