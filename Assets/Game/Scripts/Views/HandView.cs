﻿using Scripts.Models.Cards;
using Scripts.Models.Events;
using strange.extensions.mediation.impl;
using Scripts.Utils.Constants;
using UnityEngine;
using UnityEngine.UI;

namespace Scripts.Views
{
    public class HandView 
        : View
        , ISubscriber<BattleEnded>
        , ISubscriber<CardPicked>
        , ISubscriber<RespawnCanvasRequested> 
    {
        public Canvas ParentCanvas;
        private bool _subscribed;

        [Inject]
        public Hand Hand { get; set; }

        public GameObject[] CardSlots = new GameObject[Constants.MaximumHandSize];

        protected override void Awake()
        {
            UpdateHandView(this, new Card[Constants.MaximumHandSize]);
            HideCards();
        }

        public void OnEvent(CardPicked e)
        {
            ShowCards();
        }

        public void Update()
        {
            if (!_subscribed && Hand != null)
            {
                Hand.HandChanged += UpdateHandView;
                _subscribed = true;
            }
        }

        private void UpdateHandView(object sender, Card[] cards)
        {
            for (var i = 0; i < Constants.MaximumHandSize; i++)
            {
                var image = CardSlots[i].GetComponent<Image>();
                var cardView = CardSlots[i].GetComponentInChildren<CardView>();
                if (cards[i] == null)
                {
                    image.enabled = false;
                    image.sprite = null;
                    cardView.Card = null;
                }
                else
                {
                    image.enabled = true;
                    image.sprite = Resources.Load
                    (string.Format("Cards/Images/{0}", cards[i].Title)
                    , typeof(Sprite)
                    ) as Sprite;
                    cardView.Card = cards[i];
                }
                
            }
            
        }

        public void OnEvent(BattleEnded e)
        {
            HideCards();
            for (var i = 0; i < gameObject.transform.childCount; i++)
            {
                Destroy(gameObject.transform.GetChild(i).gameObject);
            }
        }

        public void OnEvent(RespawnCanvasRequested e)
        {
            HideCards();
        }

        private void HideCards()
        {
            ParentCanvas.enabled = false;
        }

        private void ShowCards()
        {
            ParentCanvas.enabled = true;
        }
    }
}
