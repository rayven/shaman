﻿using Scripts.Models.Cards;
using Scripts.Models.Events;
using strange.extensions.mediation.impl;

namespace Scripts.Views
{
    public class CardView : View, IClickable
    {
        [Inject]
        public IEventManager EventManager { get; set; }

        public Card Card { get; set; }

        public void OnClick()
        {
            EventManager.CurrentEventAggregator.Publish(new SelectRequested(Card));
        }
    }
}
