﻿using Scripts.Behaviours.Cameras;
using Scripts.Views.HealthBar;
using strange.extensions.mediation.impl;
using UnityEngine.EventSystems;

namespace Scripts.Views
{
    public class ActorView : View
        , IPointerEnterHandler
        , IPointerExitHandler
    {
        public void OnPointerEnter(PointerEventData eventData)
        {
        }

        public void OnPointerExit(PointerEventData eventData)
        {
        }
    }
}
