﻿using Scripts.Models.Events;
using Scripts.Services.Loot;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Scripts.Views
{
    public class LootOnClick : View
    {
        [Inject]
        public IEventManager EventManager { get; set; }
        public ILootable Lootable { get; set; }

        public void OnClick()
        {
            EventManager.CurrentEventAggregator.Publish(new AddLootableToBagRequested(Lootable));
            EventManager.CurrentEventAggregator.Publish(new BattleEnded());
        }
    }
}
