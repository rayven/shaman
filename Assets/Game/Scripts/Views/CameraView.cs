﻿using Scripts.Managers.Camera;
using strange.extensions.mediation.impl;

namespace Scripts.Views
{
    public class CameraView : View
    {
        [Inject]
        public ICameraManager CameraManager { get; set; }

        public void Update()
        {
            CameraManager.UpdateCurrentState();
        }
    }
}
