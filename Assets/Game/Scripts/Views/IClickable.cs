﻿namespace Scripts.Views
{
    public interface IClickable
    {
        void OnClick();
    }
}
