﻿using Scripts.Models;
using Scripts.Models.Actors;
using Scripts.Models.Events;
using strange.extensions.mediation.impl;
using UnityEngine;
using Scripts.Services.Loot;
using Scripts.Models.Elements;

namespace Scripts.Views
{
    public class BattlegroundView 
        : View, 
        ISubscriber<BattleStarted>, 
        ISubscriber<BattleEnded>,
        ISubscriber<PlayerWon>,
        ISubscriber<ShowLootRequested>
    {
        [Inject]
        public IEventManager EventManager { get; set; }
        [Inject("Maiken")]
        public Ally Player { get; set; }
        public Battleground Battleground { get; set; }
        public LootableFactory LootableFactory { get; set; }

        public Transform TornPosition;
        public Transform MaikenPosition;
        public Element Environment;

        private BoxCollider _boxCollider;


        protected override void Awake()
        {
            _boxCollider = GetComponent<BoxCollider>();
        }
        
        public void OnTriggerEnter(Collider collider)
        {
            if (collider.gameObject.CompareTag("Player"))
            {
                Battleground.TornPosition = TornPosition.position;
                Battleground.MaikenPosition = MaikenPosition.position;
                EventManager.CurrentEventAggregator = Battleground.EventAggregator;
                Battleground.Environment = Environment;
                Battleground.EventAggregator.Publish(new PlayerEntered(Environment));
            }
        }

        public void OnTriggerExit(Collider collider)
        {
            if (collider.gameObject.CompareTag("Player"))
            {
                EventManager.CurrentEventAggregator = null;
                Battleground.EventAggregator.Publish(new PlayerExited());
            }
        }
        
        public void OnEvent(BattleStarted e)
        {
            _boxCollider.enabled = false;
        }

        public void OnEvent(PlayerWon e)
        {
            //pop loot
            Destroy(gameObject);
        }

        public void OnEvent(BattleEnded e)
        {
            if (_boxCollider != null)
            {
                _boxCollider.enabled = true;
            }
        }

        public void OnEvent(ShowLootRequested e)
        {
            e.Lootables.ForEach(lo => {
                ThrowInTheAir(lo);
            });

            EventManager.CurrentEventAggregator.Publish(new BattleEnded());
        }

        private void ThrowInTheAir(ILootable lootable) {
            var rigidbody = LootableFactory.Create(lootable, transform.position).GetComponent<Rigidbody>();
            rigidbody.AddForce(Vector3.up *1000);
        }
    }
}
