﻿using UnityEngine;

namespace Scripts.Views
{
    public class InstantParticleView : MonoBehaviour
    {
        private ParticleSystem _particleSystem;

        public void Awake()
        {
            _particleSystem = GetComponentInChildren<ParticleSystem>();
        }

        public void Update()
        {
            if (!_particleSystem.IsAlive())
            {
                Destroy(gameObject);
            }
        }
    }
}
