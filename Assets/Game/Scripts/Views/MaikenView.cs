﻿using Scripts.Behaviours.Cameras;
using Scripts.Models.Actors;
using Scripts.Models.Entities;
using Scripts.Models.Events;
using Scripts.Utils.Constants;
using Scripts.Utils.Enums;
using UnityEngine;

namespace Scripts.Views
{
    public class MaikenView 
        : AllyView
        , IClickable
        , ICardPickable
        , ISubscriber<BattleStarted>
        , ISubscriber<BattleEnded>
    {
        [Inject("Maiken")]
        public Ally Ally { get; set; }

        public Transform HandsPosition;
        public Transform CooldownIndicator;
        public GameObject Targetable;

        protected override void Awake()
        {
            Controller = GetComponent<CharacterController>();
        }

        protected override void Start()
        {
            Ally.StateChanged += UpdateState;
        }

        public void Update()
        {
            Marker.SetActive(Ally.IsSelected && Ally.IsNotOnCooldown);
            Targetable.SetActive(Ally.IsTargetable);

            Ally.Position = new Vector3(transform.position.x, transform.position.y - 0.5f, transform.position.z);
            Ally.HandsPosition = HandsPosition.position;
            Ally.CooldownIndicatorPosition = CooldownIndicator.position;
            Ally.Update();
        }

        private void ResetAnimator()
        {
            Animator.SetBool(AnimatorParameters.IsIdle, false);
            Animator.SetBool(AnimatorParameters.IsRunning, false);
            Animator.SetBool(AnimatorParameters.IsDead, false);
        }

        private void UpdateState(object sender, ActorState state)
        {
            ResetAnimator();
            switch (state)
            {
                case ActorState.Idle:
                    Animator.SetBool(AnimatorParameters.IsIdle, true);
                    break;
                case ActorState.Running:
                    Animator.SetBool(AnimatorParameters.IsRunning, true);
                    break;
                case ActorState.Dead:
                    Animator.SetBool(AnimatorParameters.IsDead, true);
                    break;
                case ActorState.Battle:
                    Animator.SetBool(AnimatorParameters.IsIdle, true);
                    break;
            }
        }

        public void OnClick()
        {
            EventManager.CurrentEventAggregator.Publish(new SelectRequested(Ally));
        }

        public void PickCard()
        {
            EventManager.CurrentEventAggregator.Publish(new AllyPickCardRequested(Ally, Enums.AllyType.Maiken));
        }

        public void OnEvent(BattleStarted e)
        {
            var currentRadius = Controller.radius;
            Controller.radius = currentRadius * 3;
            Controller.center = new Vector3(Controller.center.x, Controller.center.y + 0.05f, Controller.center.z);
        }

        public void OnEvent(BattleEnded e)
        {
            var currentRadius = Controller.radius;
            Controller.radius = currentRadius / 3;
            Controller.center = new Vector3(Controller.center.x, Controller.center.y - 0.05f, Controller.center.z);
        }
    }
}
