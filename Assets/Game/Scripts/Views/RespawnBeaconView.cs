﻿using Scripts.Managers.Respawn;
using strange.extensions.mediation.impl;
using Scripts.Utils.Constants;
using UnityEngine;

namespace Scripts.Views
{
    public class RespawnBeaconView : View
    {
        [Inject]
        public RespawnManager RespawnManager { get; set; }

        private bool _isDisabled = true;
        private Texture _activeTexture;
        private ushort _childIndex;
        private float _timeStamp;

        protected override void Awake()
        {
            _activeTexture = Resources.Load(TexturePaths.InukshukDiffuse2, typeof(Texture)) as Texture;
            _childIndex = 0;
        }

        public void OnTriggerEnter(Collider collider)
        {
            if (collider.gameObject.CompareTag("Player"))
            {
                RespawnManager.LastRespawnPosition = transform.position;
                _isDisabled = false;
            }
        }

        public void Update()
        {
            if (_isDisabled) return;
            if (_childIndex >= transform.childCount) return;
            if (Time.time < _timeStamp) return;
            
            var renderer = transform.GetChild(_childIndex).GetComponent<Renderer>();
            renderer.material.SetTexture("_MainTex", _activeTexture);
            _childIndex++;
            _timeStamp = Time.time + 0.5f;

        }
    }
}
