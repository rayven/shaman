﻿using System;
using System.Linq;
using UnityEngine;

namespace Scripts.Views.Particles
{
    public class ParticleCallbackBehaviour : MonoBehaviour
    {
        private ParticleSystem[] _particleSystem;
        public Action Callback;

        public void Start()
        {
            _particleSystem = GetComponentsInChildren<ParticleSystem>();
        }

        public void Update()
        {
            if (_particleSystem.Any(x => x.IsAlive())) return;
            
            Callback();
            Destroy(gameObject);
        }
    }
}
