﻿using System.Linq;
using UnityEngine;

namespace Scripts.Views.Particles
{
    public class DestroyDeadChildrenParticleView : MonoBehaviour
    {
        private ParticleSystem[] _particleSystems;

        public void Start()
        {
            _particleSystems = GetComponentsInChildren<ParticleSystem>();
        }

        public void Update()
        {
            if (_particleSystems.Any(p => p.IsAlive())) return;

            Destroy(gameObject);
        }
    }
}
