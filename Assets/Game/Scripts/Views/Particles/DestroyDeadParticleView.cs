﻿using UnityEngine;

namespace Scripts.Views.Particles
{
    public class DestroyDeadParticleView : MonoBehaviour
    {
        private ParticleSystem _particleSystem;

        public void Start()
        {
            _particleSystem = GetComponent<ParticleSystem>();
        }

        public void Update()
        {
            if (!_particleSystem.IsAlive())
            {
                Destroy(gameObject);
            }
        }
    }
}
