﻿using Scripts.Models.Actors;
using Scripts.Utils.Extensions;
using UnityEngine;
using Scripts.Models.Events;
using UnityTest;
using UnityEngine.UI;

namespace Scripts.Views.HealthBar
{
    public class OrthographicHealthBarView : MonoBehaviour
        , ISubscriber<BattleStarted>
        , ISubscriber<BattleEnded>
    {
        public Actor Actor { get; set; }
        public Transform HealthBarPosition;

        private Camera _camera;
        private Camera _guiCamera;

        public RectTransform HealthBar;
        public RectTransform DetailedHealthBar;
        public RectTransform FullDetailedHealthBar;


        public void Start()
        {
            Actor.HealthChanged += UpdateHealthBar;
            _camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
            _guiCamera = GameObject.FindGameObjectWithTag("GuiCamera").GetComponent<Camera>();
            gameObject.SetActive(false);

            FullDetailedHealthBar.gameObject.SetActive(true);
            DetailedHealthBar.gameObject.SetActive(false);
        }
        
        public void UpdateHealthBar(object sender, long currentHealth)
        {
            SetNormalizedHealthToHealthBars(Actor.GetNormalisedHealth());
        }

        private void SetNormalizedHealthToHealthBars(float normalizedHealth)
        {
            HealthBar.localScale = new Vector3(normalizedHealth, 1, 1);
            if (normalizedHealth >= 1)
            {
                FullDetailedHealthBar.gameObject.SetActive(true);
                DetailedHealthBar.gameObject.SetActive(false);
            }
            else
            {
                FullDetailedHealthBar.gameObject.SetActive(false);
                DetailedHealthBar.gameObject.SetActive(true);
                DetailedHealthBar.localScale = new Vector3(normalizedHealth, 1, 1);
            }
        }

        public void LateUpdate()
        {
            var viewPos = _camera.WorldToNormalizedViewportPoint(HealthBarPosition.position);
            transform.position = _guiCamera.NormalizedViewportToWorldPoint(viewPos);
        }

        public void OnEvent(BattleStarted e)
        {
            gameObject.SetActive(true);
        }

        public void OnEvent(BattleEnded e)
        {
            gameObject.SetActive(false);
        }
    }
}
