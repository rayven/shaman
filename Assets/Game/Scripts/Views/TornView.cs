﻿using System.Collections.Generic;
using Scripts.Models.Actors;
using Scripts.Models.Events;
using Scripts.Utils.Constants;
using Scripts.Utils.Enums;
using UnityEngine;
using Scripts.Models.Elements;

namespace Scripts.Views
{
    public class TornView 
        : AllyView
        , IClickable
        , ICardPickable
        , ISubscriber<BattleStarted>
        , ISubscriber<BattleEnded>
    {
        [Inject("Torn")]
        public Ally Ally { get; set; }

        public Transform HandsPosition;
        public RectTransform CooldownIndicator;
        public GameObject Targetable;
        public GameObject DeathToken;

        protected override void Awake()
        {
            Controller = GetComponent<CharacterController>();
        }

        public void Update()
        {
            Subscribe();

            Marker.SetActive(Ally.IsSelected && Ally.IsNotOnCooldown);
            Targetable.SetActive(Ally.IsTargetable);

            Ally.Position = transform.position;
            Ally.HandsPosition = HandsPosition.position;
                Ally.CooldownIndicatorPosition = CooldownIndicator.position;
            Ally.Update();
        }

        private void UpdateElement(object sender, KeyValuePair<Stats, Element> element)
        {
            var renderer = transform.FindChild("Model").FindChild("Body").GetComponentInChildren<Renderer>();
            var texture = Resources.Load("Allies/Torn/Torn_Stone", typeof (Texture)) as Texture;
            renderer.material.SetTexture("_MainTex", texture);
        }

        private void Subscribe()
        {
            if (!IsSubscribed)
            {
                Ally.StateChanged += UpdateState;
                Ally.ElementsSystem.ElementChanged += UpdateElement;
                DeathToken.SetActive(false);
                IsSubscribed = true;
            }
        }

        private void ResetAnimator()
        {
            Animator.SetBool(AnimatorParameters.IsDead, false);
            Animator.SetBool(AnimatorParameters.IsIdle, false);
        }

        private void UpdateState(object sender, ActorState state)
        {
            ResetAnimator();
            switch (state)
            {
                case ActorState.Idle:
                    Animator.SetBool(AnimatorParameters.IsIdle, true);
                    break;
                case ActorState.Dead:
                    Animator.SetBool(AnimatorParameters.IsDead, true);
                    DeathToken.SetActive(true);
                    break;
                case ActorState.Battle:
                    Animator.SetBool(AnimatorParameters.IsIdle, true);
                    break;
            }
        }

        public void OnClick()
        {
            EventManager.CurrentEventAggregator.Publish(new SelectRequested(Ally));
        }

        public void PickCard()
        {
            EventManager.CurrentEventAggregator.Publish(new AllyPickCardRequested(Ally, Enums.AllyType.Torn));
        }

        public void OnEvent(BattleStarted e)
        {
            gameObject.SetActive(true);
            DeathToken.SetActive(false);
        }

        public void OnEvent(BattleEnded e)
        {
            gameObject.SetActive(false);
            DeathToken.SetActive(false);
        }
    }
}
