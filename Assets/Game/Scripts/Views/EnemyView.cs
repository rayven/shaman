﻿using Scripts.Models.Actors;
using Scripts.Models.Events;
using Scripts.Utils.Constants;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Scripts.Views
{
    public class EnemyView 
        : View
        , IClickable
        , ISubscriber<BattleStarted>
        , ISubscriber<BattleEnded>
    {
        private BoxCollider _boxCollider;
        private Animator _animator;
        private NavMeshAgent _agent;
        
        public IEventAggregator EventAggregator { get; set; }

        public Enemy Enemy { get; set; }
        public EnemyType Type;
        public Transform BattlePosition;
        public Transform TornPosition;
        public Transform BattlegroundPosition;
        public Transform HandsPosition;
        public GameObject Targetable;
        public GameObject DeathToken;

        private bool _isSubscribed;
        
        protected override void Start()
        {
            _boxCollider = GetComponent<BoxCollider>();
            _animator = GetComponent<Animator>();
            _agent = GetComponent<NavMeshAgent>();
            Enemy.BattlePosition = BattlePosition.position;
            Enemy.CurrentTargetPosition = TornPosition.position;
        }

        private void Subscribe()
        {
            if (!_isSubscribed)
            {
                Enemy.StateChanged += UpdateState;
                DeathToken.SetActive(false);
                _isSubscribed = true;
            }
        }

        private void UpdateState(object sender, ActorState state)
        {
            DeathToken.SetActive(state == ActorState.Dead);
            _animator.SetBool(AnimatorParameters.IsChasing, state == ActorState.Chase);
            _animator.SetBool(AnimatorParameters.IsPatroling, state == ActorState.Patrol);
            _animator.SetBool(AnimatorParameters.IsIdle, state == ActorState.Battle);
            _animator.SetBool(AnimatorParameters.IsDead, state == ActorState.Dead);
        }

        public void Update()
        {
            Subscribe();

            _agent.speed = Enemy.Speed;
            Targetable.SetActive(Enemy.IsTargetable);
            Enemy.Position = transform.position;
            Enemy.HandsPosition = HandsPosition.position;
            Enemy.Update();
        }

        public void OnClick()
        {
            EventAggregator.Publish(new SelectRequested(Enemy));
        }
        
        public void OnEvent(BattleEnded e)
        {
            DeathToken.SetActive(false);
            var currentSize = _boxCollider.size;
            _boxCollider.size = new Vector3(currentSize.x * 4f, currentSize.y, currentSize.z * 4f);
        }

        public void OnEvent(BattleStarted e)
        {
            DeathToken.SetActive(false);
            var currentSize = _boxCollider.size;
            _boxCollider.size = new Vector3(currentSize.x / 4f, currentSize.y, currentSize.z / 4f);
        }
    }

    public enum EnemyType
    {
        Uraki,
        Wendigo
    }
}
