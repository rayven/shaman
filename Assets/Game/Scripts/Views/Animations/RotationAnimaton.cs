﻿using UnityEngine;

namespace Scripts.Views.Animations
{
    public class RotationAnimaton : MonoBehaviour
    {
        private float _degree;
        private float _timeSpan;

        public void Start()
        {
            _degree = 1.0f;
            _timeSpan = Time.time;
        }

        public void Update()
        {

                transform.Rotate(new Vector3(0, 0, 1), _degree);

        }

        private void ResetTimeSpan()
        {
            _timeSpan = Time.time + 0.1f;
        }
    }
}
