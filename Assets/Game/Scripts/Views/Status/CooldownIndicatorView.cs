﻿using Scripts.Models.Actors;
using UnityEngine;
using UnityEngine.UI;

namespace Scripts.Views.Status
{
    public class CooldownIndicatorView : MonoBehaviour
    {
        public Actor Actor { get; set; }

        private Image _image;
        private bool _isCooldownStarted;
        private float _timeSpan;
        private float _cooldown;

        protected void Awake()
        {
            _image = GetComponent<Image>();
        }

        public void Start()
        {
            gameObject.SetActive(false);
            Actor.CooldownStarted += StartCooldown;
        }

        private void StartCooldown(object sender, float cooldown)
        {
            gameObject.SetActive(true);
            _timeSpan = Time.time + cooldown;
            _cooldown = cooldown;
            _isCooldownStarted = true;
        }

        public void Update()
        {
            if (!_isCooldownStarted) return;

            if (Time.time > _timeSpan || Actor.IsNotOnCooldown)
            {
                _image.fillAmount = 1;
                gameObject.SetActive(false);

                Actor.ResetCooldown();
            }

            _image.fillAmount = GetNormalizedElapseTime();
        }

        private float GetNormalizedElapseTime()
        {
            return (_timeSpan - Time.time) / _cooldown;
        }
    }
}
