﻿using UnityEngine;

namespace Scripts.Views
{
    public class MaikenEyeView : MonoBehaviour
    {
        public SkinnedMeshRenderer MeshRenderer;

        public void Update()
        {
            transform.position = MeshRenderer.sharedMesh.vertices[1000] + new Vector3(1,1,1);
        }
    }
}
