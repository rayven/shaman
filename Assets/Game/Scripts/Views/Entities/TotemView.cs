﻿using System.Collections.Generic;
using Assets.Game.Scripts.Models.Entities;
using Assets.Game.Scripts.Models.Resolvers.Builders;
using Scripts.Models;
using Scripts.Models.Events;
using Scripts.Models.Targetabilities;
using Scripts.Models.viewModels;
using Scripts.Utils.Constants;
using Scripts.Views;
using UnityEngine;

namespace Assets.Game.Scripts.Views.Entities
{
    public class TotemView
        : MonoBehaviour
        , ISelectable
    {
        
        private List<GameObject> _parts;
        private Transform _transform;
        public Totem Totem { get; private set; }

        public void Awake()
        {
            _parts = new List<GameObject>();
            _transform = GetComponent<Transform>();
        }

        public void Attach(Totem totem)
        {
            Totem = totem;
            Totem.TotemPartsUpdated += UpdateTotem;
            gameObject.SetActive(false);
        }

        public void UpdateTotem(object sender)
        {
            var totemPart = GameObject.Instantiate(Resources.Load(Paths.TotemPart, typeof (GameObject))) as GameObject;
            _parts.Add(totemPart);

            for (var i = 0; i < _parts.Count; i++)
            {
                var part = _parts[i];
                part.transform.position = new Vector3(
                      _transform.position.x
                    , _transform.position.y + ((i+1) * part.transform.localScale.y)
                    , _transform.position.z);
            }
        }

        public TargettingCriterions SelfCriterions { get; private set; }
        public TargettingCriterions TargetCriterions { get; private set; }
        public Allegiance Allegiance { get; private set; }

        public bool IsSelected { get; set; }
        public bool IsTargetable { get; set; }
        public void Select()
        {
            throw new System.NotImplementedException();
        }

        public void UnSelect()
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<IRequiredTargets> GetActionResolvers()
        {
            throw new System.NotImplementedException();
        }
    }
}
