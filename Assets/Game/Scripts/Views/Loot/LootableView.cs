﻿using Scripts.Models.Events;
using Scripts.Services.Loot;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Scripts.Views.Loot
{
    public class LootableView : View
    {
        [Inject]
        public IEventManager EventManager { get; set; }
        public ILootable Lootable { get; set; }

        public void OnTriggerEnter(Collider collider)
        {
            if(collider.gameObject.tag == "Player")
            {
                EventManager.CurrentEventAggregator.Publish(new AddLootableToBagRequested(Lootable));
                Destroy(gameObject);
            }
        }
    }
}
