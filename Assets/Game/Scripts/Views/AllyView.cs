﻿using Scripts.Models.Events;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Scripts.Views
{
    public class AllyView : ActorView
    {
        [Inject]
        public IEventManager EventManager { get; set; }

        protected CharacterController Controller;
        public Animator Animator;
        protected bool IsSubscribed;

        public GameObject Marker;
    }
}
