﻿using Scripts.Behaviours.Movement;
using Scripts.Utils.Extensions;
using System;
using UnityEngine;

namespace Scripts.Views
{
    public class ProjectileSpellBehaviour : MonoBehaviour
    {
        public Vector3 Destination { get; set; }
        public Action Callback { get; set; }

        private const int SPELL_SPEED = 20;
        private IMoveBehaviour _moveBehaviour;
        private ParticleSystem _particleSystem;

        public void Start()
        {
            _moveBehaviour = GetComponent<IMoveBehaviour>();
            _particleSystem = GetComponent<ParticleSystem>();
        }

        public void Update()
        {
            _moveBehaviour.Move(Destination, SPELL_SPEED);
            if (transform.position.Near(Destination))
            {
                Destroy(gameObject);
                Callback();
            }
        }
    }
}
