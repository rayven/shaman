﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Assets.Game.Scripts.Models.Cards;
using System.IO;
using Scripts.Models.Cards;
using Scripts.Utils.Constants;

namespace Assets.Game.Scripts.DAL
{
    class CardSerializer
    {
        public void SerializeCatalog(Catalog catalog)
        {
            //var serialized = JsonConvert.SerializeObject(catalog.AllCards.Values);

            //File.WriteAllText(SupportFilesPath.MainCardbase, serialized);
        }

        public IEnumerable<Card> ReadMainbase()
        {
            var rawCards = File.ReadAllText(SupportFilesPath.MainCardbase);
            IEnumerable<Card> cards = JsonConvert.DeserializeObject<IEnumerable<Card>>(rawCards, new JsonSerializerSettings() { ContractResolver = JsonPrivateResolver.Contract });

            return cards;
        }
    }
}
