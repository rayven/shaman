﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;

namespace Assets.Game.Scripts.DAL
{
    public class JsonPrivateResolver : DefaultContractResolver
    {
        private JsonPrivateResolver() { }

        private static JsonPrivateResolver _instance;
        public static DefaultContractResolver Contract
        {
            get
            {
                if (_instance == null) { _instance = new JsonPrivateResolver(); }
                return _instance;
            }
        }

        protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
        {
            var prop = base.CreateProperty(member, memberSerialization);

            if (!prop.Writable)
            {
                var property = member as PropertyInfo;
                if (property != null)
                {
                    var hasPrivateSetter = property.GetSetMethod(true) != null;
                    prop.Writable = hasPrivateSetter;
                }
            }

            return prop;
        }
    }
}
