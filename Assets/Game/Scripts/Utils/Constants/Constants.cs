﻿using System.IO;

namespace Scripts.Utils.Constants
{
    public static class Constants
    {
        public const float GroundDistance = 1.5f;
        public const float AttackCooldown = 8f;
        public const float CardPickCooldown = 5f;
        public const ushort MaximumHandSize = 5;
        public const float Gravity = 20.0f;
    }

    public static class LootableTypes
    {
        public const string Card = "CardLootable";
    }

    public static class ParticlePaths
    {
        public const string LeavesCaster = "Particles/Healing_Caster";
        public const string LeavesReceive = "Particles/Healing_Receive";
        public const string CooldownReset = "Particles/Cooldown_Reset";
        public const string FireBall = "Particles/Fireball";
        public const string Explosion = "Particles/Fireball-explosion";
        public const string DarkFireball = "Particles/Dark_Fireball";
        public const string DarkExplosion = "Particles/Dark_Fireball_Explosion";
    }

    public static class TexturePaths
    {
        public const string InukshukDiffuse1 = "Textures/Inukshuk/Tex_Inukshuk_Diffuse";
        public const string InukshukDiffuse2 = "Textures/Inukshuk/Tex_Inukshuk_Diffuse2";
    }

    public static class Paths
    {
        public const string LootButton = @"Interfaces/LootPhase/Button";
        public const string LootCanvas = @"Interfaces/LootPhase/LootCanvas";

        public const string RespawnCanvas = @"Interfaces/RespawnCanvas";
        public const string PickPhaseCanvas = @"Interfaces/PickPhaseCanvas";

        public const string BattleStartDecks = @"Interfaces/BattleStart/BattleStartDecks";
        public const string HandCanvas = @"Interfaces/Hand/HandCanvas";

        public const string TornModel = @"Allies/Torn/Torn";
        public const string MaikenModel = @"Allies/Maiken/Maiken";

        public const string Camera = @"Cameras/Camera";

        public const string TotemBase = @"Totems/Base";
        public const string TotemPart = @"Totems/Part";
    }

    public static class AnimatorParameters
    {
        public const string IsDead = @"IsDead";
        public const string IsIdle = @"IsIdle";
        public const string IsRunning = @"IsRunning";
        public const string IsPatroling = @"IsPatroling";
        public const string IsChasing = @"IsChasing";
        
        public const string Attack = @"Attack";
        public const string Cast = @"Cast";
        public const string Jump = @"Jump";
        public const string HighJump = @"HighJump";
    }

    public static class AnimatorState
    {
        public const string Attack = @"Attack";
        public const string Spell = @"Spell";
    }

    public static class SupportFilesPath
    {
        private const string _root = @".\Support Files\";
        private const string _cardbase = @"Cardbase";
        private const string _mainbase = @"Main.cards";

        public static string Cardbases { get { return Path.Combine(_root, _cardbase); } }
        public static string MainCardbase { get { return Path.Combine(Cardbases, _mainbase); } }
    }

}
