﻿using System;
using UnityEngine;

namespace Scripts.Utils.Extensions
{
    public static class Vector3Extensions
    {
        private const float TOLERANCE = 0.5f;

        public static bool Near(this Vector3 position, Vector3 otherPosition)
        {
            return (Math.Abs(position.x - otherPosition.x) < TOLERANCE &&
                    Math.Abs(position.z - otherPosition.z) < TOLERANCE);
        }
    }
}
